<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Catalogue extends CI_Controller {

		
		public function index()
		{
			$image['image'] = $this->Users->findAllUsersBd();
			// print_r($image);
			$this->load->view('CATALOGUE/index');
			$this->load->view('CATALOGUE/navigation');
			// $this->load->view('ADMIN/home');
			$this->load->view('CATALOGUE/AddCat');
			$this->load->view('CATALOGUE/ListCat');
			$this->load->view('CATALOGUE/footer');
		
		}


		public function AddCat()
		{

			$this->load->view('CATALOGUE/index');
			$this->load->view('CATALOGUE/navigation');
			$this->load->view('CATALOGUE/AddCat');
			$this->load->view('CATALOGUE/footer');
		

		}

	
		public function ListeCat(){
		 	$data['allCategorie']=$this->Categorie->findAllCategorieBd();
			$this->load->view('CATALOGUE/index');
			$this->load->view('CATALOGUE/navigation');
			$this->load->view('CATALOGUE/ListeCat',$data);
			$this->load->view('CATALOGUE/footer');
	
		}

	}

?>  