
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Utilisateur extends CI_Controller {

		
		public function index()
		{
			$image['image'] = $this->Users->findAllUsersBd();
			$this->load->view('USERS/index');
			$this->load->view('USERS/navigation');
			$this->load->view('USERS/home');
			$this->load->view('ADMIN/footer');
		
		}

		public function creerEvenement()
		{
			$this->load->view('WELCOME/header',array('titre'=>'Créer un Evènement'));
			$data['categorie'] = $this->Categorie->findAllCategorieBd();
			$this->load->view('USERS/index');
			$this->load->view('USERS/navigation');
			// $this->load->view('USERS/home');
			$this->load->view('EVENEMENT/AddEvent',$data);
			$this->load->view('ADMIN/footer');
		}

		public function mesevenement(){

			$data['categorie'] = $this->Categorie->findAllCategorieBd();
			$data['donnees'] = $this->Event->findAllEvenementBd();
			$this->load->view('USERS/index');
			$this->load->view('USERS/navigation');
			$this->load->view('EVENEMENT/listeEvent',$data);
			$this->load->view('ADMIN/footer');
		}

		public function Addparticipant(){ // on indique juste une adresse email
			if(isset($_SESSION['CLIENT']) && isset($_POST['email']) && isset($_POST['id_event']) ){
				//on cree le user correspondant
				$data['nom'] =  $_POST['email'];
				$data['email'] =  $_POST['email'];
				$this->Users->hydrate($data);
				$this->Users->addUser();

				// on cree un participant
				$data['id_users'] = $this->Users->findUsersId();
				$data['password'] = '1234-'.$data['id_users']; // un modele aleatoire pourra etre cree
				$data['id_event'] = $_POST['id_event'];

				//on ajoute le participant
				$this->Participant->hydrate($data);
				$this->Participant->addParticipant();
			}
		}

}
