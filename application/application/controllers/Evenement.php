<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Evenement extends CI_Controller {

		public function creerEvenement()
		{	
			if (isset($_POST['categorie']) && $_FILES['image']['error'] == 0) {
				$data['id_categorie'] = $this->Categorie->findIdCategorie($_POST['categorie']);
				print_r($data['id_categorie']);
				echo "ici<br><br>";
				print_r($_SESSION);
				if ($_FILES['image']['size'] <= 100000000){

                    $infosfichier =pathinfo($_FILES['image']['name']);   // separe les noms et extensions de fichier
                    $extension_upload = $infosfichier['extension'];

                    $config =$infosfichier['filename'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i');
					$ma_variable = str_replace('.', '_', $config);
					$ma_variable = str_replace(' ', '_', $config);
					$config = $ma_variable.'.'.$extension_upload;
					move_uploaded_file($_FILES['image']['tmp_name'],'assets/images/even_img/'.$config);


					$data['image']=$config;
					echo "la data cat img";
					print_r($data);
					echo "la data cat img";
                }

				$data['nom'] = $_POST['nom'];
				$data['description'] = $_POST['description'];
				$data['id_client'] = $_POST['id_client'];
				$data['nb_participant'] = $_POST['nb_participant'];
				$data['date_debut'] = $_POST['date_debut'];
				$data['date_fin'] = $_POST['date_fin'];
				$data['heure_debut'] = $_POST['heure_debut'];
				$data['heure_fin'] = $_POST['heure_fin'];
				$data['lieu'] = $_POST['lieu'];
				$data['etat'] = 'En Cours';

				echo "<br><br>la data final";
				print_r($data);
				echo "la data final";
				$this->Event->hydrate($data);
				$this->Event->addEvenement();

				//avant la redirection on gere les niveaux des utilisateurs (Tout utilisateur creant un evenement devien client)
				if (isset($_SESSION['VISITEUR']) && $_SESSION['VISITEUR'] == 'ok') {	//cas du visiteur
					echo "cas d'un visiteur";
					$client['id_users'] = $_SESSION['USER']['id'];
					$client['email'] = $_SESSION['USER']['email'];
					$this->Visiteur->hydrate(array('email'=>$_SESSION['USER']['email']));
					$client['password'] = $this->Visiteur->findPassword($_SESSION['USER']['email']);
					//ajout du client
					$this->Client->hydrate($client);
					$this->Client->addClient();
					//modification de son niveau en tant que user
					$this->Users->hydrate(array('email'=>$_SESSION['USER']['email']));
					$this->Users->modifyNiveau(2);
					$_SESSION['USER']['niveau'] = 2;
					// unset($_SESSION['Visiteur']);
					$_SESSION['Visiteur'] = 'non';
					$_SESSION['CLIENT'] = 'ok';
					//suppression en tant que visiteur
					$this->Visiteur->hydrate(array('email'=>$_SESSION['USER']['email']));
					// $this->Visiteur->moveto('upgraded');
					$this->Visiteur->remove($_SESSION['USER']['email']);

				}

				if (isset($_SESSION['participant']) && $_SESSION['participant'] == 'ok') {	//cas du participant
					$client['id_users'] = $_SESSION['USER']['id'];
					$client['email'] = $_SESSION['USER']['email'];
					$this->Participant->hydrate(array('email'=>$_SESSION['USER']['email']));
					$client['password'] = $this->Participant->findPassword($_SESSION['USER']['email']);
					//ajout du client
					$this->Client->hydrate($client);
					$this->Client->addClient();
					//modification de son niveau en tant que user
					$this->Users->hydrate($_SESSION['USER']['email']);
					$this->Users->modifyNiveau(2);
					$_SESSION['USER']['niveau'] = 2;
					// unset($_SESSION['PARTICIPANT']);
					$_SESSION['PARTICIPANT'] = 'non';
					$_SESSION['CLIENT'] = 'ok';
					//suppression en tant que participant
					// $this->Participant->hydrate(array('email'=>$_SESSION['USER']['email']));
					// $this->Participant->remove($_SESSION['USER']['email']);
				}
				

				//redirection vers le dashbord client
				redirect(site_url(array('Client_controller','index')));
			}else{
				echo "Un probleme est survenu";
			}

		}

		public function updateEvenement(){
			if (isset($_POST['id_event'])) {
				$data['id'] = $_POST['id_event'];

				if (isset($_POST['categorie'])) {
					$data['categorie'] = $_POST['categorie'];

					$this->Event->hydrate($data);
					$this->Event->actualiseCategorie();
				}

				if (isset($_POST['nom'])) {
					$data['nom'] = $_POST['nom'];

					$this->Event->hydrate($data);
					$this->Event->actualiseNom();
				}

				if (isset($_POST['description'])) {
					$data['description'] = $_POST['description'];

					$this->Event->hydrate($data);
					$this->Event->actualiseDescription();
				}

				if (isset($_POST['nb_participant'])) {
					$data['nb_participant'] = $_POST['nb_participant'];

					$this->Event->hydrate($data);
					$this->Event->actualiseNb_participant();
				}

				if (isset($_POST['date_debut'])) {
					$data['date_debut'] = $_POST['date_debut'];

					$this->Event->hydrate($data);
					$this->Event->actualiseDate_debut();
				}

				if (isset($_POST['date_fin'])) {
					$data['date_fin'] = $_POST['date_fin'];

					$this->Event->hydrate($data);
					$this->Event->actualiseDate_fin();
				}

				if (isset($_POST['heure_debut'])) {
					$data['heure_debut'] = $_POST['heure_debut'];

					$this->Event->hydrate($data);
					$this->Event->actualiseHeure_debut();
				}

				if (isset($_POST['heure_fin'])) {
					$data['heure_fin'] = $_POST['heure_fin'];

					$this->Event->hydrate($data);
					$this->Event->actualiseHeure_fin();
				}

				if (isset($_POST['lieu'])) {
					$data['lieu'] = $_POST['lieu'];

					$this->Event->hydrate($data);
					$this->Event->actualiseLieu();
				}

				if ( isset($_FILES['image']) ){
					if ( ($_FILES['image']['error'] == 0)  && $_FILES['image']['size'] <= 100000000){

                        $infosfichier = pathinfo($_FILES['image']['name']);   // separe les noms et extensions de fichier
                        $extension_upload = $infosfichier['extension'];

                        $config = $infosfichier['filename'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i');
 						$ma_variable = str_replace('.', '_', $config);
						$ma_variable = str_replace(' ', '_', $config);
						$config = $ma_variable.'.'.$extension_upload;
						move_uploaded_file($_FILES['image']['tmp_name'],'assets/images/even_img/'.$config);
						/***************
						**************************************************************************************************
							************* ICI il faut l'instruction pour suppimer l'ancien fichier *********************
						**************************************************************************************************
						*************************************/

					$data['image'] = $config;

					$this->Event->hydrate($data);
					$this->Event->actualiseImage();
					
                    }else{

						$_SESSION['alerte'] = 'Votre fichier image est corrompu, Vérifiez le et reessayez!!';
						redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription

                    }
                }
			}
			
		}

		public function deleteEvenement($id_event){
			
			$this->Event->hydrate(array('id'=>$id_event));
			$this->Event->delete();
		}



	}
