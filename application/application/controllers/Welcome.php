<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Welcome extends CI_Controller {

		// public $dataUser;
		// public $dataClient;
		
		public function index()
		{

		 redirect(site_url(array('Home','index')));

		}


		public function renseigneinscription()
		{
			$this->load->view('WELCOME/header');
			$this->load->view('WELCOME/inscriptionUser');
			$this->load->view('WELCOME/footer');
		}

		public function renseigneconnexion()
		{
			$this->load->view('WELCOME/header');
			$this->load->view('WELCOME/InscriptionUser');
			$this->load->view('WELCOME/footer');
		}

		public function ceerEvenement()
		{
			$this->load->view('WELCOME/header',array('titre'=>'Créer un Evènement'));
			$this->load->view('EVENEMENT/AddEvent');
			$this->load->view('WELCOME/footer');
		}

		public function manageInscription()
		{	

			if(isset($_SESSION['USER']) || isset($_SESSION['ADMIN'])){ 
				session_destroy();
				$this->Welcome->index();
			}else{

				if (isset($_POST['nom']) && isset($_POST['telephone']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['password_confirm'])) {

					if($_POST['password']==$_POST['password_confirm']){

						$dataUser['nom'] = $_POST['nom'];
						$_SESSION['nom'] = $_POST['nom'];
						$dataUser['telephone'] = $_POST['telephone'];
						$_SESSION['telephone'] = $_POST['telephone'];
						$dataClient['email'] = $_POST['email'];
						$_SESSION['email'] = $_POST['email'];
						$dataClient['password'] = $_POST['password'];
						$_SESSION['password'] = $_POST['password'];
						$dataUser['date_inscription'] = date("d-m-Y H:i:s");

					}else{
						$_SESSION['message'] = 'La Confirmation Du Mot De Passe Est Incorrecte';
						redirect(site_url(array('Welcome','renseigneinscription')));
						// $this->renseigneinscription();
					}

					if ( isset($_FILES['profil']) ){
						if ( $_FILES['profil']['error'] == '0' ){
		        			if (deplace_fichier($_FILES)) {
		        				$dataUser['profil']=deplace_fichier($_FILES);
								$_SESSION['profil']=$dataUser['profil'];
		        			}
								
		                    else{
								// $this->Welcome->renseigneinscription('');
								$_SESSION['message'] = 'La taille du fichier choisi  est très grande veuillez le remplacer svp !!';
								redirect(site_url(array('Welcome','renseigneinscription')));

		                        // $data['message']='non';
		                    }
		                }else{
		                	$_SESSION['message'] = 'L\'image choisie  est endommagée  veuillez la remplacer svp !!';
							redirect(site_url(array('Welcome','renseigneinscription')));
							// $this->renseigneinscription();
							
		                }
		            }

		            if ($this->Client->testExistemail($dataClient['email']) == 'ok') {
		            	$_SESSION['message'] = 'Cette addresse mail existe deja, Veuillez renseigner une autre addresse';
							redirect(site_url(array('Welcome','renseigneinscription')));
		            }
		            // print_r($dataUser);
		            $dataClient['id_users'] = $this->Users->addUser($dataUser);
		            echo('<p>Id correspondant:'.$dataClient['id_users'].'</p>');
		            $this->Client->addClient($dataClient);
		            // $this->ceerEvenement();
		            redirect(site_url(array('Welcome','ceerEvenement')));

				}else{
					$_SESSION['message'] = 'Une erreur est survenue pendant le transfert';
					redirect(site_url(array('Welcome','renseigneinscription')));
				}
			
			}
			
		}

		public function deplace_fichier($fichier){
			// Testons si le fichier n'est pas trop gros
			if ($fichier['profil']['size'] <= 100000000){
		     // Testons si l'extension est autorisée
				$infosfichier = pathinfo($fichier['profil']['name']);
				$extension_upload = $infosfichier['extension'];

				$config =$fichier['profil']['name'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i');
				$ma_variable = str_replace('.', '_', $config);
				$ma_variable = str_replace(' ', '_', $config);
				$config = $ma_variable.'.'.$extension_upload;
				move_uploaded_file($fichier['profil']['tmp_name'],'assets/images/user_profil/'.$config);

				return $config;
			}else{
				return FALSE;
			}
		}
		
	}
?>  