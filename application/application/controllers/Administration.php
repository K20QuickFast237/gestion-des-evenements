<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administration extends CI_Controller {


		
			
	public function index(){
		
		if (isset($_SESSION['ADMIN'])) {
		 	$data['allCategorie']=$this->Categorie->findAllCategorieBd();
		 	$data['listeUsers'] = $this->Users->findAllUsersBd();
			$data['evenement'] = $this->Event->findAllEvenementBd();
		 	// print_r ($data ['allCategorie']);
		 	
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/home');
			$this->load->view('ADMIN/ListeCat',$data);
			$this->load->view('ADMIN/ListeUser',$data);
			$this->load->view('ADMIN/EEC',$data);
			$this->load->view('ADMIN/footer');
		
				
		}else{
	   	 session_destroy();
		 redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	public function ListeUser(){
		if (isset($_SESSION['ADMIN'])) {
		 	$data['listeUsers'] = $this->Users->findAllUsersBd();
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/ListeUser',$data);
			$this->load->view('ADMIN/footer');
		
				
		}else{
	   	 session_destroy();
		 redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	public function AddCat(){
		if (isset($_SESSION['ADMIN'])) {

			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/AddCat');
			$this->load->view('ADMIN/footer');
		
				
		}else{
	   	 session_destroy();
		 redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	public function ListeCat(){
		if (isset($_SESSION['ADMIN'])) {
		 	$data['allCategorie']=$this->Categorie->findAllCategorieBd();
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/ListeCat',$data);
			$this->load->view('ADMIN/footer');
		
				
		}else{
	   	 session_destroy();
		 redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	public function EEC(){
		if (isset($_SESSION['ADMIN'])) {
			$data['evenement'] = $this->Event->findAllEvenementBd();
		 	$data['listeUsers'] = $this->Users->findAllUsersBd();
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation',$data);
			$this->load->view('ADMIN/EEC');
			$this->load->view('ADMIN/footer');
		
				
		}else{
	   	 session_destroy();
		 redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}
	 		
	// fonction qui charge le formulaire de connexion pour un administrateur
	public function formulaireConnexion(){
		
		if (isset($_SESSION['ADMIN'])) {
			// if (isset($_SESSION['ADMIN'])) {
				   
			// }else{
			// 	session_destroy();
			// 	redirect(site_url(array('Administration','formulaireConnexion')));
			// }
		}else{
			session_destroy();
			$this->load->view('gestion_admin/formulaire_connexion');
		}
	}
	
	public function manageConnexion(){
		
		if (isset($_POST['email']) && isset($_POST['password'])) {
			
			$admin = $this->Admin->findAllAdminBd();
			// print_r($admin);
			for ($i=0; $i < $admin['total']; $i++) { 
				if ($admin[$i]['email'] == $_POST['email'] && $admin[$i]['password'] == $_POST['password']) {
					$_SESSION['ADMIN'] = $admin[$i];
				}
			}
			
			if (isset($_SESSION['ADMIN'])) {
				$numUser = $_SESSION['ADMIN']['id_users'];
				$details = $this->Users->findUsersInfo($numUser);
				$_SESSION['ADMIN']['nom'] = $details['nom'];
				$_SESSION['ADMIN']['telephone'] = $details['telephone'];
				$_SESSION['ADMIN']['profil'] = $details['profil'];
				// array_push($_SESSION['ADMIN'],$this->Users->findUsersdetails($numUser));
				redirect(site_url(array('Administration','index')));
			}else{
				$_SESSION['ERR'] = 'Les paramtres recus ne correspondent a auccun adminstrateur dans notre Database.<br> <b>Veillez recommencer SVP</b>';
				redirect(site_url(array('Administration','formulaireConnexion')));
			}
		}else{
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	// Gestions des Administrateurs
	public function manageAdmin(){
		
		if (isset($_SESSION['ADMIN'])) {
			
			$data['AllAdmin']=$this->Admin->findAllAdminBd();
			$this->load->view('WELCOME/index',$data);
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/home_admin');
			$this->load->view('WELCOME/footer');
			
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	public function testExitAdmin($email){
        $etat=0;
        $data['infoAdmin']=$this->Admin->findAllAdminBd();
        if ($data['infoAdmin']['total']<=0) {
            
        }else{
            for ($i=0; $i <$data['infoAdmin']['total'] ; $i++) { 
                if ($data['infoAdmin'][$i]['email']==$email) {
                    $etat=1;
                    break;
                }else{
                    $etat=0;
                }
            }
        }
        return $etat;
	}
	
	public function addAdmin(){
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST)) {
				$etat=$this->testExitAdmin($_POST['email']);
				if ($etat==0) {
					if (isset($_FILES['profil']) AND $_FILES['profil']['error'] == 0 ){
	        		// Testons si le fichier n'est pas trop gros
	                    if ($_FILES['profil']['size'] <= 100000000){
	                        // Testons si l'extension est autorisée
	                        $infosfichier =pathinfo($_FILES['profil']['name']);
	                        $extension_upload = $infosfichier['extension'];

	                        $config =$_FILES['profil']['name'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').$_SESSION['ADMIN']['id'];
	 						$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable.'.'.$extension_upload;
							move_uploaded_file($_FILES['profil']['tmp_name'],'assets/images/user_profil/'.$config);
							$data['profil']=$config;
							
	                    }else{
	                        $data['message_save']="La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
	                        $data['message']='non';
	                    }
	                }else{
	                    $data['message_save']="L'image choisie  est endommagée  veuillez le remplacer svp !!";
	                    $data['message']='non';
	                }
					
					$data['nom']=$_POST['nom'];
					$data['email']=$_POST['email'];
					$data['password']=$_POST['password'];
					$data['telephone']=$_POST['telephone'];
					
					$data['date']=date('Y-m-d H:i:s');
					$this->Admin->hydrate($data);
					$this->Admin->addAdmin();
					$_SESSION['message_save']="Administrateurs enregistré avec success !!";
			 		$_SESSION['success']='ok';
			 		redirect(site_url(array('Administration','manageAdmin')));
					
				}else{
					session_destroy();
					direct(site_url(array('Administration','formulaireConnexion')));
				}
			}else{
				session_destroy();
				redirect(site_url(array('Administration','formulaireConnexion')));
			}
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	public function newCategorie(){
		
		$data['nom']=$_POST['nom'];
		$data['description']=$_POST['description'];
		// $data['image']=$_FILES['image'];
		if (isset($_FILES['image']) AND $_FILES['image']['error'] == 0 ){ 
		// Testons si le fichier n'est pas trop gros
            if ($_FILES['image']['size'] <= 100000000){
                // Testons si l'extension est autorisée

		$infosfichier =pathinfo($_FILES['image']['name']);
        $extension_upload = $infosfichier['extension'];
        $config =$_FILES['image']['name'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i');
		$ma_variable = str_replace('.', '_', $config);
		$ma_variable = str_replace(' ', '_', $config);
		$config = $ma_variable.'.'.$extension_upload;

		move_uploaded_file($_FILES['image']['tmp_name'],'assets/images/cat_img/'.$config);
		echo('<p>ff</p>');
		$data['image']=$config;
		$data['etat']='actif';
		print_r($data);
		$this->Categorie->hydrate($data);
		$this->Categorie->addCategorie();
		redirect(site_url(array('Administration','index')));

			}
		}
	}

	public function deconnexion(){
		if (isset($_SESSION['ADMIN'])) {
			session_destroy();
		}
		session_destroy();
		redirect(site_url(array('Administration','index')));
	}


}
