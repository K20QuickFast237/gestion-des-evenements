<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Users_model extends CI_Model{
		
		function __construct()
			{
			
			}
		
			// gerer un admin

			protected $id;
			protected $nom;
			protected $telephone;
			protected $profil;
			protected $email;
			protected $niveau;
			protected $date_inscription;
			protected $date_modification_profil;

			protected $table= 'users';

		// hydratation, c'est une fonction de filtrage necessaire avant la manipulation des donnees

			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

		// retourne le nombre d'elements d'une liste passee en parametre

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}



			public function modifyUser($data){
			    // $this->db->set('id', $this->id)
			    $this->db->set('nom', $data['nom'])
			    	->set('telephone', $data['telephone'])
			    	->set('profil', $data['profil'])
			    	->where('id',$data['id'])
			    	// ->set('email', $data['email'])
			    	// ->set('niveau', $data['niveau'])
			    	// ->set('date_inscription', $data['date_inscription'])
					->update($this->table);

			 }

			public function addUser(){
			    // $this->db->set('id', $this->id)
			    $this->db->set('nom', $this->nom)
				       	 ->set('telephone', $this->telephone)
				       	 ->set('profil', $this->profil)
				       	 ->set('email', $this->email)
				       	 ->set('niveau', $this->niveau)
				    	// ->set('date_inscription', $data['date_inscription'])
					   	 ->insert($this->table);

		
			}
			
			public function modifyNiveau($niveau){
			    // $this->db->set('id', $this->id)
			    $this->db->set('niveau', $niveau)
			    		 ->where('email', $this->email)
					   	 ->update($this->table);

		
			}


			// fonction qui charge tous les users pour faire le filtrage de donnee
			
			public function findAllUsersBd(){
				$data = $this->db->select('id,nom,email,niveau,profil,date_inscription,date_modification_profil')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['email']=$row->email;
			       	$donnees[$i]['niveau']=$row->niveau;
			       	$donnees[$i]['profil']=$row->profil;
			       	$donnees[$i]['date_inscription']=$row->date_inscription;
			       	$donnees[$i]['date_modification_profil']=$row->date_modification_profil;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}


			// fonction qui verifie l'existance d'un email parmi les Users

			public function testExistemail($email){
				
				$data =$this->db->select('email')
								->from($this->table)
								->where('email', $email)
								->limit(1)
								->get()
								->result();

								
				foreach ($data as $row){
			       	$donnees['email']=$row->email;
				}

				if (isset($donnees)) {
					return 'ok';
				}else{
					return 'non';
				}
			}

			// fonction qui verifie l'existance d'un telephone parmi les Users

			public function testExistelephone($telephone){
				
				$data =$this->db->select('telephone')
								->from($this->table)
								->where('telephone', $telephone)
								->limit(1)
								->get()
								->result();

								
				foreach ($data as $row){
			       	$donnees['telephone']=$row->telephone;
				}

				if (isset($donnees)) {
					return 'ok';
				}else{
					return 'non';
				}
			}

			// fonction qui retourne tous les parametres utilisateur
			
			public function findUsersdetails($num){
				
				$donnees = $this->Users->findAllUsersBd();
				if ($donnees['data']=='ok') {
					return $donnees[$num-1];
				}
			}

			// fonction qui reccupère juste l'ID d'un utilisateur

			// public function findUsersId($data){
			// 	$data =$this->db->select('id')
			// 					->from($this->table)
			// 					->where('email' , $data['email'])
			// 					->limit(1)
			// 					->get()
			// 					->result();

			// 	foreach ($data as $row){
			//        	$donnees['id']=$row->id;
			// 	}

			// 	return $donnees['id'];
			// }

			// fonction qui reccupère juste l'ID d'un utilisateur

			public function findUsersId(){
				$data =$this->db->select('id')
								->from($this->table)
								->where('email' , $this->email)
								->limit(1)
								->get()
								->result();

				foreach ($data as $row){
			       	$donnees['id']=$row->id;
				}

				return $donnees['id'];
			}


			// fonction qui reccupère toutes les infos d'un utilisateur

			public function findUsersInfo($id){
				$data =$this->db->select('id,nom,telephone,profil,email,niveau,date_inscription,date_modification_profil')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();
					
				$donnees['data'] = 'non';
								
				foreach ($data as $row){
			       	$donnees['id']=$row->id;
			       	$donnees['nom']=$row->nom;
			       	$donnees['telephone']=$row->telephone;
			       	$donnees['profil']=$row->profil;
			       	$donnees['email']=$row->email;
			       	$donnees['niveau']=$row->niveau;
			       	$donnees['date_inscription']=$row->date_inscription;
			       	$donnees['date_modification_profil']=$row->date_modification_profil;
			       	$donnees['data']='ok';
				}
				
				return $donnees;

			}

			// fonction qui reccupère juste le nom d'un utilisateur

			public function findUsersNom($id){
				$data =$this->db->select('nom')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();
								
				foreach ($data as $row){
			       	$donnees['nom']=$row->nom;
				}

				return $donnees['nom'];
			}

			// fonction qui reccupère juste le Telephone d'un utilisateur

			public function findUserTelephone($id){
				$data =$this->db->select('Telephone')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

					$donnees['Telephone']='non';				
				foreach ($data as $row){
			       	$donnees['Telephone']=$row->Telephone;
				}

				return $donnees['Telephone'];
			}
			

		    // fonction qui reccupère juste le date_inscription d'un utilisateur
			
			public function findUsersDate_inscription($id){
				$data =$this->db->select('date_inscription')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

				foreach ($data as $row){
			       	$donnees['date_inscription']=$row->date_inscription;
				}

				return $donnees['date_inscription'];
			}

			// fonction qui reccupère juste le profil d'un utilisateur
			
			public function findUsersProfil($id){
				$data =$this->db->select('profil')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();
						
				foreach ($data as $row){
			       	$donnees['profil']=$row->profil;
				}

				return $donnees['profil'];
			}

			// fonction qui reccupère juste le date_modification_profil d'un utilisateur
			
			public function findUsersDate_modification_profil($id){
				$data =$this->db->select('date_modification_profil')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();
						
				foreach ($data as $row){
			       	$donnees['date_modification_profil']=$row->date_modification_profil;
				}

				return $donnees['date_modification_profil'];
			}





			// setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setNom($nom){
				$this->nom=$nom;
			}

			public function setEmail($email){
				$this->email=$email;
			}

			public function setNiveau($niveau){
				$this->niveau=$niveau;
			}
			
			public function setProfil($profil){
				$this->profil=$profil;
			}

			public function setDate_inscription($date_inscription){
				$this->date_inscription=$date_inscription;
			}
			public function setTelephone($telephone){
				$this->telephone=$telephone;
			}
			public function setDate_modification_profil($date_modification_profil){
				$this->date_modification_profil=$date_modification_profil;
			}
			

			// getteurs

			public function getId(){
				return $this->id;
			
			}

			
			public function getNom(){
				return $this->nom;
			
			}
			
			public function getEmail(){
				return $this->email;
			
			}

			public function getNiveau(){
				return $this->niveau;
			
			}

			public function getDate_inscription(){
				return $this->date_inscription;
			
			}

			public function getDate_modification_profil(){
				return $this->date_modification_profil;
			
			}
			public function getTelephone(){
				return $this->telephone;
			
			}
				public function getProfil(){
				return $this->profil;
			
			}
						
	
}


?>
