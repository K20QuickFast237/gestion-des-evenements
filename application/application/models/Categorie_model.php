<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Categorie_model extends CI_Model{
		
		function __construct()
			{
			
			}
		
			// gerer un Categorie

			private $id;
			private $nom;
			private $description;
			private $image;
			private $etat;

			protected $table= 'categorie';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}



			public function addCategorie(){
				print_r($this);
			    // $this->db->set('id', $this->id)
			    $this->db->set('nom', $this->nom)
			    	->set('description', $this->description)
			    	->set('image', $this->image)
			    	->set('etat', $this->etat)
					->insert($this->table);
		
			}


			// fonction qui charge toutes les Categories pour faire le filtrage de donnees
			
			public function findAllCategorieBd(){
				$data = $this->db->select('id, nom, description, image, etat')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['description']=$row->description;
			       	$donnees[$i]['image']=$row->image;
			       	$donnees[$i]['etat']=$row->etat;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}


			// fonction qui reccupère juste le nom d'une Categorie

			public function findNomCategorie($id){
				$data =$this->db->select('nom')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

					$donnees['nom']='non';				
				foreach ($data as $row){
			       	$donnees['nom']=$row->nom;
				}

				return $donnees['nom'];
			}
			

		    // fonction qui reccupère juste la description d une Categorie
			
			public function findIdUsers($id){
				$data =$this->db->select('description')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

				foreach ($data as $row){
			       	$donnees['description']=$row->description;
				}

				return $donnees['description'];
			}

			// fonction qui reccupère juste  l'image d'une Categorie
			
			public function findImageCategorie($id){
				$data =$this->db->select('image')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();
						
				foreach ($data as $key){
			       	$donnees['image']=$key->image;
				}

				return $donnees['image'];
			}

			// fonction qui reccupère juste  l'id d'une Categorie
			
			public function findIdCategorie($nom){
				$data =$this->db->select('id')
								->from($this->table)
								->where('nom', $nom)
								->limit(1)
								->get()
								->result();
						
				foreach ($data as $key){
			       	$donnees['id']=$key->id;
				}

				return $donnees['id'];
			}



			// setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setNom($nom){
				$this->nom=$nom;
			}
			
			public function setDescription($description){
				$this->description=$description;
			}

			public function setImage($image){
				$this->image=$image;
			}

			public function setEtat($etat){
				$this->etat=$etat;
			}
			

			// getteurs

			public function getId(){
				return $this->id;
			}

			
			public function getNom(){
				return $this->nom;
			}

			public function getDescription(){
				return $this->description;
			}

			public function getImage(){
				return $this->image;
			}	

			public function getEtat(){
				return $this->etat;
			}		
	
}

?>
