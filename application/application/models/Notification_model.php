<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Notification_model extends CI_Model{
		
		function __construct()
			{
			
			}
		
			// gerer un Notification

			private $id;
			private $id_participant;
			private $id_event;
			private $date;
			private $libelle;

			protected $table= 'Notification';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}



			public function addNotification(){

			    $this->db->set('id', $this->id)
			    	->set('id_participant', $this->id_participant)
			    	->set('id_event', $this->id_event)
			    	->set('date', $this->date)
			    	->set('libelle', $this->libelle)
					->insert($this->table);
		
			}


			// fonction qui charge tous les Notification pour faire le filtrage de donnee
			
			public function findAllNotificationBd(){
				$data = $this->db->select('id,id_participant,id_event,date,libelle')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_participant']=$row->id_participant;
			       	$donnees[$i]['id_event']=$row->id_event;
			       	$donnees[$i]['date']=$row->date;
			       	$donnees[$i]['libelle']=$row->libelle;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}

			// fonction qui reccupère juste l'identifiant d'un evenement

			public function findIdEvent($id){
				$data =$this->db->select('id_event')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

								
				foreach ($data as $row){
			       	$donnees['id_event']=$row->id_event;
				}

				return $donnees['id_event'];
			}

			// fonction qui reccupère juste l identifiant d in participant

			public function findIdParticipat($id){
				$data =$this->db->select('id_participant')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

					$donnees['id_participant']='non';				
				foreach ($data as $row){
			       	$donnees['id_participant']=$row->id_participant;
				}

				return $donnees['id_participant'];
			}
			

		    // fonction qui reccupère juste la date d entree d'un Notification
			
			public function findDate($id){
				$data =$this->db->select('date')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

				foreach ($data as $row){
			       	$donnees['date']=$row->date;
				}

				return $donnees['date'];
			}

			// fonction qui reccupère juste  le libelle d'un Notification
			
			public function findLibelle($id){
				$data =$this->db->select('libelle')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();
						
				foreach ($data as $row){
			       	$donnees['libelle']=$row->libelle;
				}

				return $donnees['libelle'];
			}


			// fonction qui reccupère juste le password et email d'un Notification

			// public function FindNotificationInfo($email,$password){
			// 	$data =$this->db->select('password,email')
			// 			->from($this->table)
			// 			->where(array('password'=>$password,'email'=>$email))
			// 			->limit(1)
			// 			->get()
			// 			->result();

			// 	$donnees['data']='non';			
			// 	foreach ($data as $row){
			//        	$donnees['email']=$row->email;
			//        	$donnees['password']=$row->password;
			//        	$donnees['data']='ok';
			// 	}

			// 	return $donnees;
			// }



			// setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setIdParticipant($id_participant){
				$this->id_participant=$id_participant;
			}
			
			public function setDate($date){
				$this->date=$date;
			}

			public function setLibelle($libelle){
				$this->libelle=$libelle;
			}
			public function setIdEvent($id_event){
				$this->id_event=$id_event;
			}
			

			// getteurs

			public function getId(){
				return $this->id;
			
			}

			
			public function getIdParticipant(){
				return $this->id_participant;
			
			}

			public function getDate(){
				return $this->date;
			
			}

			public function getLibelle(){
				return $this->libelle;
			
			}
			public function getIdEvent(){
				return $this->id_event;
			
			}
						
	
}

?>
