  
  
<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li><a href="<?php echo site_url(array('Administration','ListeCat')); ?>"><i class="fa fa-renren text-red"></i> <span> Home</span></a></li>
      
      <li class="treeview  menu-open">
        <a href="#">
          <i class="fa fa-users text-green"></i>
          <span>Gestion des utilisateurs</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url(array('Administration','ListeUser')); ?>"><i class="fa fa-users"></i>Liste des utilisateurs</a></li>
        </ul>
      </li>
       <li class="treeview  menu-open  ">
        <a href="#">
          <i class="fa fa-th-large text-blue"></i>
          <span>Gestion de Catégories</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url(array('Administration','AddCat')); ?>"><i class=" ion-android-add-circle"></i> Ajouter une Catégorie</a></li>
          <li><a href="<?php echo site_url(array('Administration','ListeCat')); ?>"><i class="fa fa-wpforms"></i>Liste des Catégories</a></li>
        </ul>
      </li>
       <li class="treeview  menu-open ">
        <a href="<?php echo site_url(array('Administration','EEC')); ?>"><i class="fa fa-xing-square text-yellow"></i> <span>Evènement en Cours</span></a></li>>
          
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
       
      </li>
    
    </ul>
  </section>
</aside>

  
