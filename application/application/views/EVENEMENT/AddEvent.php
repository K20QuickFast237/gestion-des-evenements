
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class=" col-md-offset-3 col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Création d'un nouvel évènement</font></font></h3>
          </div>
          <!-- /.box-header -->

          <!-- message d erreur -->
          <?php 
          if (isset($_session['message'])) {
            echo "<p>".$_session['message']."</p>";
          }
          ?>
          <!-- form start -->
          <form role="form" enctype="multipart/form-data" method="post" action="<?php echo site_url(array('Evenement','creerEvenement')); ?>">
            <div class="box-body">
              <div class="form-group">
                <label for="Inputcat_Event">
                  <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Catégorie de l'évènement</font></font>
                </label>
                <select name="categorie" required="">
                  <option disabled="" selected=""  value=""> Choisir une categorie </option>
                  <?php   
                  for ($i=0; $i < $categorie['total']; $i++) { 
                    ?>  
                    <option value="<?php  echo($categorie[$i]['nom']); ?>"><?php  echo($categorie[$i]['nom']); ?> </option>

                    <?php       
                  }
                  ?>
                </select>
              </div>
              <div class="form-group">
                <label for="InputName_Event">
                  <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nom de l'évènement</font></font>
                </label>
                <input type="text" class="form-control" name="nom" placeholder="Entrez le nom de l'évènement">
              </div>
              <div class="form-group">
                <label for="TextareaDescription">
                  <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Description</font></font>
                </label>
                <textarea rows="10" class="form-control" name="description"required="" ></textarea>
              </div>
              <div class="form-group">
                <label for="InputImgEvent">
                  <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Image de l'évènement</font></font>
                </label>
                <input type="file" name="image" onchange="loadFile(event)"required="" >
                <p class="help-block"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Image décrivant votre évènement.</font></font></p>
              </div>
              <div style="width: 100px; height: 100px;">
                <img style="width: 100px; height: 100px;" id="img">
              </div>
              <div class="form-group">
                <label for="Inputparticipant_Event">
                  <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nombre de participant à l'évènement</font></font>
                </label>
                <input type="number" class="form-control" name="nb_participant" required="" placeholder="Entrez le nombre de participant à l'évènement">
              </div>
              <div class="form-group">
                <label for="InputDate1_Event">
                  <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Date de début de l'évènement</font></font>
                </label>
                <input type="date" class="form-control" name="date_debut" required="" placeholder="Entrez la date de début de l'évènement">
              </div>
              <div class="form-group">
                <label for="InputDate2_Event">
                  <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Date de fin de l'évènement</font></font>
                </label>
                <input type="date" class="form-control" name="date_fin" required="" placeholder="Entrez la date de fin de l'évènement">
              </div>
              <div class="form-group">
                <label for="Inputheure1_Event">
                  <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Heure de debut de l'évènement</font></font>
                </label>
                <input type="time" class="form-control" name="heure_debut" required="" placeholder="Entrez l'heure de debut de l'évènement">
              </div>
              <div class="form-group">
                <label for="Inputheure2_Event">
                  <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Heure de fin de l'évènement</font></font>
                </label>
                <input type="time" class="form-control" name="heure_fin" required="" placeholder="Entrez l'heure de fin de l'évènement">
              </div>
              <div class="form-group">
                <label for="InputLieu_Event">
                  <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Lieu de l'évènement</font></font>
                </label>
                <input type="text" class="form-control" name="lieu" required="" placeholder="Entrez lieu de l'évènement">
              </div>
            </div>
            <input type="hidden" name="id_client" value="<?php echo($_SESSION['USER']['id']) ?>">
            <!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Valider</font></font></button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

<script>
  var loadFile = function(event) {
    var profil = document.getElementById('img');
    profil.src = URL.createObjectURL(event.target.files[0]);
  };

</script>
















































