 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        DASHBOARD
      </h1>
      <?php if (isset($session['participant'])) {?>
        <ol class="breadcrumb">
          <li><a href="<?php echo site_url(array('Client_controller','mesinvitation')); ?>"><i class="fa fa-dashboard"></i> HOME/Mes Evenements</a></li>
        </ol>
      <?php }else{ ?>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(array('Client_controller','mesevenement')); ?>"><i class="fa fa-dashboard"></i> HOME/Mes Evenements</a></li>
      </ol>
    <?php } ?>
    </section>
        <!-- Main content -->
  <section class="content w3l-mag-main">
  <!--/mag-content-->
    <div class="mag-content-inf py-4">
      <div class="container">

        <?php 
          for ($j=0; $j <$donnees['total'] ; $j++) { ?>

          <div class="maghny-grids-inf row">
            <div class="maghny-gd-1 col-lg-4 col-md-6">
              <div class="maghny-grid">
                <figure class="effect-lily">
                  <img class="img-fluid" src= "<?php echo img_url('Even_img/'.$donnees[$j]['image']); ?>">
                  <figcaption>
                    <div >
                      <h3 class="top-text"><?php echo $donnees[$j]['nom']; ?></h3>
                      <h4><?php  for ($i=0; $i < $categorie['total']; $i++) {echo $categorie[$i]['nom'];} ?> </h4>
                    </div>
                  </figcaption>
                </figure>
              </div>
              <a href="#" class="read-more btn mt-3">Vue Détaillée</a>
            </div>
          </div>
      <?php } ?>
  <!--//mag-content-->
  </section>
</div>