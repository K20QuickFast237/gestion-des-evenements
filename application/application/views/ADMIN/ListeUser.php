
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        GESTION USERS
        <small>CONTROL PANEL</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> LISTE USERS</a></li>
         
      </ol>
    
<table id="myTable" class="dataTableS_filter table-responsive two-axis">
  
  <thead>
    <th>N°</th>
    <th scope="col" >Photo de profil</th>
    <th>Nom</th>
    <th>Email</th>
    <th>Actions</th> 
  </thead>
  <tbody>
    <?php
      //if ($allContact['data']=='ok') {
      $i=0 ;
        while ($i<$listeUsers['total']) { ?>
          <tr>
            <td><?php echo $i+1 ?></td>
            <td><img src="<?php echo img_url('user_profil/'.$listeUsers[$i]['profil']);?>" style="width:50px; height:50px;border-radius: 50%;"></td>
            <td><?php echo ($listeUsers[$i]['nom']); ?></td>
            <td><?php echo ($listeUsers[$i]['email']); ?></td>
            <td>
              <div style="font-size: 1px;"> 
                      <form enctype="multipart/form-data" method="post" action="Management.php" style=" display: inline-block;">
                        <a href="#">
                          <input type="hidden" name="N1"  value="<?php //echo $user[$i]['id']; ?>">
                          <input type="hidden" name="N2"  value="consulter">
                          <button  type="submit" style="background-color:inherit; border: none;">
                            <span class="fa fa-eye editer" style="color:aqua; font-size: 12px;"> </span>
                          </button>
                        </a>
                      </form>
                      <form enctype="multipart/form-data" method="post" action="Management.php" style=" display: inline-block;">
                        <a href="#">
                          <input type="hidden" name="N1"  value="<?php //echo $user[$i]['id']; ?>">
                          <input type="hidden" name="N2"  value="editer">
                          <button  type="submit" style="background-color:inherit; border: none;">
                            <span class="fa fa-pencil editer" style="color:blue; font-size: 12px;">  </span>
                          </button>
                        </a>
                      </form>
                      <?php if($listeUsers[$i]['niveau'] <=2 ){ ?>
                      <form enctype="multipart/form-data" method="post" action="Management.php" style=" display: inline-block;">
                        <a href="#">
                          <input type="hidden" name="N1"  value="<?php //echo $user[$i]['id']; ?>">
                          <input type="hidden" name="N2"  value="supprimer">
                          <button  type="submit" style="background-color:inherit; border: none;">
                            <span class="fa fa-trash Supprimer" style="margin-left: 10px;color:red; font-size: 12px;">  </span>
                          </button>
                        </a>
                      </form>
                      <?php } if ($listeUsers[$i]['niveau'] == 1) { ?>
                          <form enctype="multipart/form-data" method="post" action="Management.php" style=" display: inline-block;">
                            <a href="#">
                              <input type="hidden" name="N1"  value="<?php //echo $user[$i]['id']; ?>">
                              <input type="hidden" name="N2"  value="desactiver">
                              <button  type="submit" style="background-color:inherit; border: none;">
                                <span class="glyphicon glyphicon-remove-circle activer_inverse" style="margin-left: 10px;color:orange; font-size: 12px;"> </span>
                              </button>
                            </a>
                          </form>
                      <?php } else { ?>
                       <form enctype="multipart/form-data" method="post" action="Management.php" style=" display: inline-block;">
                          <a href="#">
                            <input type="hidden" name="N1"  value="<?php //echo $user[$i]['id']; ?>">
                            <input type="hidden" name="N2"  value="activer">
                            <button  type="submit" style="background-color:inherit; border: none;">
                              <span class="glyphicon glyphicon-ok activer" style="margin-left: 10px;color:green; font-size: 12px;"> </span>
                            </button>
                          </a>
                       </form>
                        </div>
                      <?php } ?>

                    </td>
          </tr>
    <?php  $i++ ;  } // else{ } ?>
  </tbody>
</table>