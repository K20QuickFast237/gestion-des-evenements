
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        DASHBOARD
      </h1>
      <?php if (isset($session['participant'])) {?>
        <ol class="breadcrumb">
          <li><a href="<?php echo site_url(array('Client_controller','mesinvitation')); ?>"><i class="fa fa-dashboard"></i> HOME</a></li>
        </ol>
      <?php }else{ ?>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(array('Client_controller','mesevenement')); ?>"><i class="fa fa-dashboard"></i> HOME</a></li>
      </ol>
    <?php } ?>
    </section>
  </div>