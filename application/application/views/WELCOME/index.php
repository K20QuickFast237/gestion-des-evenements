<header>
	<!-- <div class="container">
            
        </div> -->

	<div class="container">
		<!-- top header -->
		<section class="row top_header pt-3">
			<div class="col-lg-6 buttons ml-auto">
				<p style="margin-right: 20%;"><span class="fa fa-phone"></span> +237 620080998</p>
				<!-- <a  class="btn btn-info btn-lg-block w3ls-btn px-sm-4 px-3 text-capitalize mr-sm-2" href="#login-popup" style="margin-left: 10px;" >Connexion</a> -->
				<a class="btn btn-info1 btn-lg-block px-sm-4 px-3 text-capitalize  pull-right" href="#register-popup">Connexion</a>
			</div>
		</section>
		<!-- top header -->
		<!-- nav -->
		<nav class="py-3">
			<div id="logo">
				<h1>
					<a class="navbar-brand" href="#"> <span class="fa fa-empire"></span>GESTION<span><span class="line"></span>EVENEMENTS</span>
					</a>
				</h1>
			</div>

			<label for="drop" class="toggle"><span class="fa fa-bars"></span></label>
			<input type="checkbox" id="drop" />
			<ul class="menu mt-2">
				<li class="mr-lg-3 mr-2 active"><a href="#">Accueil</a></li>
				<li class="mr-lg-3 mr-2"><a href="#foot">A Propos </a></li>
				<li class="mr-lg-3 mr-2"><a href="#ser">Services</a></li>
				<!-- <li class="mr-lg-3 mr-2"><a href="gallery.html">Evenements</a></li> -->
				<li class="mr-lg-3 mr-2 pb-0">
					<!-- First Tier Drop Down -->
					<label for="drop-2" class="toggle">Evenements <span class="fa fa-angle-down" aria-hidden="true"></span> </label>
					<a href="#">Evenements <span class="fa fa-angle-down" aria-hidden="true"></span></a>
					<input type="checkbox" id="drop-2" />
					<ul class="drop-down-ul">
						<li><a href="error.html">ARCHIVES</a></li>
						<li><a href="events.html">CATALOGUE</a></li>
					</ul>
				</li>
				<li><a href="#foot">Contact</a></li>
			</ul>
		</nav>
		<!-- //nav -->
	</div>
</header>

<section class="banner layer" id="home">
	<div class="container">
		<div class="banner-text">
			<div class="slider-info mb-4">
				<div class="banner-heading">
					<h3>
						UNE NOUVELLE VISION D'ORGANISATION EVENEMENTIELLE
					</h3>
				</div>
				<a href="#"> Creez Votre Evenement</a>
			</div>
			<!-- To bottom button-->
			<div class="thim-click-to-bottom">
				<div class="rotate">
					<a href="#newevent" class="scroll">
						<span class="fa fa-angle-double-down"></span>
					</a>
				</div>
			</div>
			<!-- //To bottom button-->

		</div>
	</div>
</section>
< <div class="blog-section py-5" style="margin-bottom:85px;">
	<div class="container py-md-5 py-4">
		<div id="newevent" class="waviy text-center mb-md-5 mb-4" style="margin-top: 50px; margin-bottom:40px; background-color: #17a2b8;color: white;font-size: 30px;">

			NOUVEAUX EVENEMENTS
		</div>

		<div class="row">
			<div class="col-lg-4 col-md-6 item">
				<div class="card">
					<div class="card-header p-0 position-relative">
						<a href="#">
							<img class="card-img-bottom d-block radius-image-full" src="../assets/images/blog1.jpg" alt="Card image cap">
						</a>
					</div>
					<div class="card-body blog-details">
						<span class="label-blue">Ceremonies du nouvel an </span>
						<a href="#" class="blog-desc">Ceremonies du nouvel an
						</a>

					</div>
					<div class="ser-button mt-4">
						<a href="#">En savoir plus</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 mt-md-0 mt-sm-5 mt-4">
				<div class="card">
					<div class="card-header p-0 position-relative">
						<a href="#single">
							<img class="card-img-bottom d-block radius-image-full" src="../assets/images/blog3.jpg" alt="Card image cap">
						</a>
					</div>
					<div class="card-body blog-details">
						<span class="label-blue">Soirees mouvementees</span>
						<a href="#single" class="blog-desc">Des sorties des plus exotiques
						</a>

					</div>
					<div class="ser-button mt-4">
						<a href="#">En savoir plus</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 mt-lg-0 mt-4">
				<div class="card">
					<div class="card-header p-0 position-relative">
						<a href="#single">
							<img class="card-img-bottom d-block radius-image-full" src="../assets/images/blog2.jpg" alt="Card image cap">
						</a>
					</div>
					<div class="card-body blog-details">
						<span class="label-blue">Mariages</span>
						<a href="#single" class="blog-desc">Des sorties des plus exotiques
						</a>

					</div>
					<div class="ser-button mt-4">
						<a href="#">En savoir plus</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>



	<!-- IMAGES -->

	<!-- IMAGES -->

	<!-- services -->
	<div class="services py-5">
		<div class="container py-md-5">
			<!-- <h3 class="heading text-capitalize mb-lg-5 mb-4" id="ser"> Nos Services </h3> -->
			<div class="waviy text-center mb-md-5 mb-4" style="margin-top: 50px; margin-bottom:40px; background-color: #17a2b8;color: white;font-size: 30px;">

				NOS SERVICES
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6 item">
					<div class="card">
						<div class="card-header p-0 position-relative">
							<a href="#">
								<img class="card-img-bottom d-block radius-image-full" src="../assets/images/g1.jpg" alt="Card image cap">
							</a>
						</div>
						<div class="card-body blog-details">
							<span class="label-blue">Ceremonies du nouvel an </span>
							<a href="#" class="blog-desc">Ceremonies du nouvel an
							</a>

						</div>
						<div class="ser-button mt-4">
							<a href="#">Cliquer ici pour plus d'informations</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 mt-md-0 mt-sm-5 mt-4">
					<div class="card">
						<div class="card-header p-0 position-relative">
							<a href="#single">
								<img class="card-img-bottom d-block radius-image-full" src="../assets/images/13.jpg" alt="Card image cap">
							</a>
						</div>
						<div class="card-body blog-details">
							<span class="label-blue">Conferences</span>
							<a href="#single" class="blog-desc">Organisez vos conferences comme des pro
							</a>

						</div>

						<div class="ser-button mt-4">
							<a href="#">Cliquer ici pour plus d'informations</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 mt-lg-0 mt-4">
					<div class="card">
						<div class="card-header p-0 position-relative">
							<a href="#single">
								<img class="card-img-bottom d-block radius-image-full" src="../assets/images/11.jpg" alt="Card image cap">
							</a>
						</div>
						<div class="card-body blog-details">
							<span class="label-blue">Anniversaires</span>
							<a href="#single" class="blog-desc">Des Anniversaires a couper le souffle
							</a>

						</div>
						<div class=" ser-button mt-4">
							<a href="#">Cliquer ici pour plus d'informations</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row service-grids">
				<a href="#">
					<div class="col-lg-3 col-sm-6">
						<div class="service-grid1">
							<span class="fa fa-globe"></span>
							<h4 class="my-3">Mariages</h4>
							<p>Le jour le plus important de votre vie organisé et géré de maniere exceptionnelle</p>
						</div>
					</div>
				</a>
				<a href="#">
					<div class="col-lg-3 col-sm-6 mt-sm-0 mt-sm-4 mt-4">
						<div class="service-grid1">
							<span class="fa fa-book"></span>
							<h4 class="my-3">Soirees</h4>
							<p>Organisez vos soirees endiablees et inoubliables autour de programmes bien organisés .</p>
						</div>
					</div>
				</a>
				<a href="#">
					<div class="col-lg-3 col-sm-6 mt-lg-0 mt-sm-4 mt-4">
						<div class="service-grid1">
							<span class="fa fa-diamond"></span>
							<h4 class="my-3">Conference</h4>
							<p>Vos conferences faites rangees reparties comme vous le souhaitez.</p>
						</div>
					</div>
				</a>
				<a href="#">
					<div class="col-lg-3 col-sm-6 mt-lg-0 mt-sm-4 mt-4">
						<div class="service-grid1">
							<span class="fa fa-book"></span>
							<h4 class="my-3">Anniversaire</h4>
							<p>Un jour par an , Un jour exceptionnel.</p>
						</div>
					</div>
				</a>
				<div class="ser-button mt-4">
					<a href="#">Tous les evenements</a>
				</div>
			</div>
		</div>


		<!-- 
			<a href="#"><div class="col-lg-2 col-md-4 col-sm-6 bg-img2" style="margin-:30%;">
				<h4>Mariage</h4>
			</div></a>
			<a href="#"><div class="col-lg-2 col-md-4 col-sm-6 bg-img3" style="margin-right:10%;" >
				<h4>Conference</h4></a>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-6 bg-img4">
				<h4>Soirees</h4>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-6 bg-img5">
				<h4>Anniversaire</h4>
			</div>
			
		</div>
	</div> -->
		<!-- </div> -->
		<!-- //services -->

		<!-- wedding date -->

		<!-- //wedding date -->

		<!-- team -->
		<section class="team-team">
			<div class="team py-5">
				<div class="container py-md-5">

					<div class="team py-5">
						<div class="container py-md-5 py-4">
							<div class="waviy white-text text-center mb-sm-5 mb-4">
								<h3 class="heading text-capitalize mb-lg-5 mb-4">Notre équipe </h3>
							</div>
							<!-- fireworks effect -->

							<div class="row team-row mt-md-5 mt-4">
								<div class="col-lg-1  col-6 team-wrap" style="width: 130px;">
									<div class="team-member text-center">
										<div class="team-img">
											<img style="border-radius: 50%; width: 150px; height: 150px; border: 7px solid #17a2b8;" src="../assets/images/t5.jpg" alt="" class="radius-image">
										</div>
										<a href="#url" class="team-title" style="position: relative;left: 27px;">Massouka Alexandre</a>
										<div class="team-details text-center">
											<div class="socials mt-20" style="position: relative;left: 27px;">
												<a href="#url">
													<span class="fa fa-facebook-f"></span>
												</a>
												<a href="#url">
													<span class="fa fa-twitter"></span>
												</a>
												<a href="#url">
													<span class="fa fa-instagram"></span>
												</a>
											</div>
										</div>
									</div>
								</div>
								<!-- end team member -->
								<div class="col-lg-1 col-6 team-wrap" style="margin-left: 35px; width: 130px;">
									<div class="team-member text-center">
										<div class="team-img">
											<img style="border-radius: 50%; width: 150px; height: 150px; border: 7px solid #17a2b8;" src="../assets/images/t5.jpg" alt="" class="radius-image">
										</div>
										<a href="#url" class="team-title" style="position: relative;left: 27px;">Tombong Kevin</a>
										<div class="team-details text-center">
											<div class="socials mt-20" style="position: relative;left: 27px;">
												<a href="#url">
													<span class="fa fa-facebook-f"></span>
												</a>
												<a href="#url">
													<span class="fa fa-twitter"></span>
												</a>
												<a href="#url">
													<span class="fa fa-instagram"></span>
												</a>
											</div>
										</div>
									</div>
								</div>
								<!-- end team member -->
								<div class="col-lg-1 col-6 team-wrap" style="margin-left: 35px; width: 130px;">
									<div class="team-member text-center">
										<div class="team-img">
											<img style="border-radius: 50%; width: 150px; height: 150px; border: 7px solid #17a2b8;" src="../assets/images/t5.jpg" alt="" class="radius-image">
										</div>
										<a href="#url" class="team-title" style="position: relative;left: 27px;">Ebanda Pascal</a>
										<div class="team-details text-center">
											<div class="socials mt-20" style="position: relative;left: 27px;">
												<a href="#url">
													<span class="fa fa-facebook-f"></span>
												</a>
												<a href="#url">
													<span class="fa fa-twitter"></span>
												</a>
												<a href="#url">
													<span class="fa fa-instagram"></span>
												</a>
											</div>
										</div>
									</div>
								</div>
								<!-- end team member -->
								<div class="col-lg-1 col-6 team-wrap" style="margin-left: 35px; width: 130px;">
									<div class="team-member text-center">
										<div class="team-img">
											<img style="border-radius: 50%; width: 150px; height: 150px; border: 7px solid #17a2b8;" src="../assets/images/t5.jpg" alt="" class="radius-image">
										</div>
										<a href="#url" class="team-title" style="position: relative;left: 27px;">Takodjou Stephane</a>
										<div class="team-details text-center">
											<div class="socials mt-20" style="position: relative;left: 27px;">
												<a href="#url">
													<span class="fa fa-facebook-f"></span>
												</a>
												<a href="#url">
													<span class="fa fa-twitter"></span>
												</a>
												<a href="#url">
													<span class="fa fa-instagram"></span>
												</a>
											</div>
										</div>
									</div>
								</div>
								<!-- end team member -->
								<div class="col-lg-1 col-6 team-wrap" style="margin-left: 35px; width: 130px;">
									<div class="team-member text-center">
										<div class="team-img">
											<img style="border-radius: 50%; width: 150px; height: 150px; border: 7px solid #17a2b8;" src="../assets/images/t4.jpg" alt="" class="radius-image">
										</div>
										<a href="#url" class="team-title" style="position: relative;left: 27px;">Ngono Appolinaire</a>
										<div class="team-details text-center">
											<div class="socials mt-20" style="position: relative;left: 27px;">
												<a href="#url">
													<span class="fa fa-facebook-f"></span>
												</a>
												<a href="#url">
													<span class="fa fa-twitter"></span>
												</a>
												<a href="#url">
													<span class="fa fa-instagram"></span>
												</a>
											</div>
										</div>
									</div>
								</div>
								<!-- end team member -->
								<div class="col-lg-1 col-6 team-wrap mt-lg-0 mt-5" style="margin-left: 35px; width: 130px;">
									<div class="team-member last text-center">
										<div class="team-img">
											<img style="border-radius: 50%; width: 150px; height: 150px; border: 7px solid #17a2b8;" src="../assets/images/t3.jpg" alt="" class="radius-image">
										</div>
										<a href="#url" class="team-title" style="position: relative;left: 27px;">Elouti Berger</a>
										<div class="team-details text-center">
											<div class="socials mt-20" style="position: relative;left: 27px;">
												<a href="#url">
													<span class="fa fa-facebook-f"></span>
												</a>
												<a href="#url">
													<span class="fa fa-twitter"></span>
												</a>
												<a href="#url">
													<span class="fa fa-instagram"></span>
												</a>
											</div>
										</div>
									</div>
								</div>
								<!-- end team member -->
								<div class="col-lg-1 col-6 team-wrap mt-lg-0 mt-5" style="margin-left: 35px; width: 130px;">
									<div class="team-member last text-center" style="height: 130px;">
										<div class="team-img">
											<img style="border-radius: 50%; width: 150px; height: 150px; border: 7px solid #17a2b8;" src="../assets/images/t1.jpg" alt="" class="radius-image">
										</div>
										<a href="#url" style="position: relative;left: 27px;">Fadhil Abouba</a>
										<div class="team-details text-center">
											<div class="socials mt-20" style="position: relative;left: 27px;">
												<a href="#url">
													<span class="fa fa-facebook-f"></span>
												</a>
												<a href="#url">
													<span class="fa fa-twitter"></span>
												</a>
												<a href="#url">
													<span class="fa fa-instagram"></span>
												</a>
											</div>
										</div>
									</div>
								</div>
								<!-- end team member -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- //team -->

		<!-- Clients -->
		<section class="clients-main">
			<div class="wthree-different-dot1 py-5">
				<div class="container py-sm-3">
					<h3 class="heading text-capitalize text-center mb-sm-5 mb-4"> Avis Des Clients </h3>
					<div class="row cli-ent">
						<div class="col-lg-4 col-md-6 item g1">
							<div class="row agile-dish-caption">
								<div class="col-lg-11 text-center mx-auto">
									<h5>Michael Johnson</h5>
									<p class="para-w3-agile"> Tres joli site et belle et tres belle plateforme pour sa gestion d'evenements</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 item g1">
							<div class="row agile-dish-caption">
								<div class="col-lg-11 text-center mx-auto">
									<h5>Mary elizabeth</h5>
									<p class="para-w3-agile"> Tres joli site et belle et tres belle plateforme pour sa gestion d'evenements.</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 item g1">
							<div class="row agile-dish-caption">
								<div class="col-lg-11 text-center mx-auto">
									<h5>Elisa kour</h5>
									<p class="para-w3-agile"> Tres joli site et belle et tres belle plateforme pour sa gestion d'evenements.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> <br>
		<!--// Clients -->

		<!-- subscribe -->

		<!-- //subscribe -->

		<!-- footer -->
		<footer>
			<div class="container py-5">
				<div class="row footer-gap">
					<div class="col-lg-4 col-sm-6">
						<h3 class="text-capitalize mb-3">Adresse</h3>
						<address class="mb-0">
							<p class=""><span class="fa fa-map-marker"></span> 24e rue AKWA<br>AKWA</p>
							<p><span class="fa fa-clock-o"></span> Horaires : 10 a.m to 6 p.m</p>
							<p><span class="fa fa-phone"></span> +237 698 54 56 12</p>
							<p><span class="fa fa-envelope-open"></span> <a href="mailto:info@example.com">event@example.com</a></p>
						</address>
					</div>

					<div class="col-lg-3 offset-lg-1 col-sm-6 mt-lg-0 mt-sm-5 mt-4 pull-right">
						<h3 class="text-capitalize mb-3" id="foot"> Suivez nous</h3>
						<p class="mb-4">Medias sociaux</p>
						<ul class="social mt-lg-0 mt-3">
							<li class="mr-1"><a href="#"><span class="fa fa-facebook"></span></a></li>
							<li class="mr-1"><a href="#"><span class="fa fa-twitter"></span></a></li>
							<li class="mr-1"><a href="#"><span class="fa fa-google-plus"></span></a></li>
							<li class=""><a href="#"><span class="fa fa-linkedin"></span></a></li>
							<li class="mr-1"><a href="#"><span class="fa fa-rss"></span></a></li>
						</ul> <br>

						<h3>A Propos de Nous</h3>
						<p style="color:white;">Jeune entreprise de gestion <br>
							d'evenement de tout genre. <br>
							Nous saurons repondre a tous <br>
							vos besoins a temps . <br>
							Nous sommes situes a AKWA RUE 1521</p>



					</div>
					<div class="col-md-3 col-sm-6 sub-two-right mt-5 ">
						<h3>Newsletter</h3>
						<form action="#" class="subscribe mb-3" method="post">
							<input type="email" name="email" placeholder="Votre adresse Email" required="">
							<button><span class="fa fa-envelope-o"></span></button>
						</form>
						<p>Entrez votre Email et recevez les dernieres informations et nouveautes.
						</p>
					</div>


				</div>
			</div>
			<div class="copyright pb-5 text-center">
				<p>© 2020 Organisation. Tous droits reserves | Developpe par <a href="http://www.W3Layouts.com" target="_blank">GESTEVENT.COM</a></p>
			</div>
		</footer>
		<!-- footer -->

		<!-- popup for login -->
		<div id="login-popup" class="popup-effect">
			<div class="popup">
				<h5 class="modal-title text-uppercase">Connexion</h5>
				<div class="login-form">
					<form action="#" method="post" class="px-3 pt-3 pb-0">
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Utilisateur</label>
							<input type="email" class="form-control" placeholder="" name="Name" id="recipient-name" required="">
						</div>
						<div class="form-group">
							<label for="recipient-name1" class="col-form-label">Mot de passe</label>
							<input type="password" class="form-control" placeholder="" name="Name" id="recipient-name1" required="">
						</div>
						<div class="right-w3l">
							<input type="submit" class="form-control" value="Se Connecter">
						</div>
					</form>

				</div>
				<a class="close" href="#">&times;</a>
			</div>
		</div>
		<!-- popup for login -->

		<!-- popup for register -->
		<div id="register-popup" class="popup-effect">
			<div class="popup">
				<h5 class="modal-title text-uppercase">Inscription</h5>
				<div class="lregister-form">
					<form action="#" method="post" class="px-3 pt-3 pb-0">
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">NOM</label>
							<input type="text" class="form-control" placeholder="" name="Name" id="recipient-name2" required="">
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">PRENOM</label>
							<input type="text" class="form-control" placeholder="" name="Name" id="recipient-name3" required="">
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Email </label>
							<input type="email" class="form-control" placeholder="" name="Name" id="recipient-name4" required="">
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Numero</label>
							<input type="text" class="form-control" placeholder="" name="Name" id="recipient-name5" required="">
						</div>
						<div class="form-group">
							<label for="recipient-name1" class="col-form-label">Mot de passe</label>
							<input type="password" class="form-control" placeholder="" name="Name" id="recipient-name6" required="">
						</div>
						<div class="right-w3l">
							<input type="submit" class="form-control" value="Get Started">
						</div>
					</form>
				</div>
				<a class="close" href="#">&times;</a>
			</div>
		</div>
		<a class="close" href="#">&times;</a>
	</div>
</div>
<button onclick="topFunction()" id="movetop" title="Go to top">
        <span class="fa fa-level-up" aria-hidden="true"></span>
    </button>
    <script>
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function () {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("movetop").style.display = "block";
            } else {
                document.getElementById("movetop").style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>

<script type="text/javascript">
		$(function(){
			$('.carousel').carousel({interval:(5000)});
		})

		         function scrollToAnyPoint (navItem) {   
	     var getAttr;   
	     $(navItem).click(function(e){    
	       e.preventDefault();     getAttr = $(this).attr('href');   var toSection =
	      $(getAttr).offset().top;    
	      $("html, body").animate({scrollTop:toSection}, 1000)   });
	      } 
	      scrollToAnyPoint('ul li a');


      jQuery(document).ready(function() {   var duration = 500;  
	       jQuery(window).scroll(function() {    
	        if (jQuery(this).scrollTop() > 100) { 
	            // Si un défillement de 100 pixels ou plus.       // Ajoute le bouton     
	              jQuery('.haut').fadeIn(duration);     } else {       // Sinon enlève le bouton     
	                jQuery('.haut').fadeOut(duration);     }   });                    jQuery('.haut').click(function(event) {     // Un clic provoque le retour en haut animé.     
	                	event.preventDefault();     jQuery('html, body').animate({scrollTop: 0}, duration);     return false;   }) })

	</script>
<!-- popup for register -->

</body>
</html>

