
  <!--header-->
  <header id="site-header" class="w3lhny-head fixed-top">
    <div class="container">
      <nav class="navbar navbar-expand-lg stroke px-0">
        <h1> <a class="navbar-brand" href="index.html">
            <span class="sublog">FIE</span>STA
          </a></h1>

      </nav>
    </div>
  </header>
  <!--/header-->
  <!--/banner-->
  <section class="w3l-main-banner" id="home">
    <div id="block" class="w3lvide-content" data-vide-bg="assets/video/fest" data-vide-options="position: 0% 50%">
      <div class="container">
        <div class="main-content">
          <h6>Le Nouvel An est la</h6>
          <h4>Get this loud and clear, only do good this new year.</h4>
          <div class="w3l-ban-icons d-flex mt-4">
            <span class="divider-separator">
            </span>
           
          </div>
          <a href="#register-popup" class="btn btn-style border-btn">Upcoming Events</a>
        </div>
      </div>
    </div>
  </section>
  <!--//banner-->
  <!-- /main-banner -->
  <div class="content-1 py-5">
    <div class="container py-md-5 py-2">
      <div class="row content-1-grids">
        <div class="col-lg-4 content-1-left forms-25-info">
          <div class="header-title">
            <span class="sub-title">New Year Eve</span>
            <h3 class="title-w3l">Life is an event. Make it memorable</h3>
          </div>
        </div>
        <div class="col-lg-4 content-1-right pl-lg-5 mt-lg-0 mt-4">
          <p class="">Lorem ipsum viverra feugiat. Pellen tesque libero ut justo,
            ultrices in ligula. Semper at tempufddfel. Lorem ipsum dolor sit amet
            elit. Non quae, fugiat nihil ad. Lorem ipsum dolor sit amet. Lorem ipsum init
            dolor sit, amet elit. Dolor ipsum non velit.
          </p>
        </div>
        <div class="col-lg-4 content-1-right pl-lg-5 mt-lg-0 mt-4">
          <p class="">Lorem ipsum viverra feugiat. Pellen tesque libero ut justo,
            ultrices in ligula. Semper at tempufddfel. Lorem ipsum dolor sit amet
            elit. Non quae, fugiat nihil ad. Lorem ipsum dolor sit amet. Lorem ipsum init
            dolor sit, amet elit. Dolor ipsum non velit.
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- //content-1-->
  <!-- /w3l-bottom-grids-6-->
  <section class="w3l-bottom-grids-6 py-5" id="features">
    <div class="container py-lg-5 py-md-4 py-2">
      <div class="grids-area-hny main-cont-wthree-fea row">
        <div class="col-lg-3 col-md-6 grids-feature">
          <div class="area-box">
            <h4><a href="#feature" class="title-head">Dj Music</a></h4>
            <span class="fa fa-music"></span>
            <p>Lorem ipsum dolor sit, consectetur adipisc elit. </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 grids-feature mt-sm-0 mt-4" data-aos="fade-down">
          <div class="area-box">
            <h4><a href="#feature" class="title-head">Drinks</a></h4>
            <span class="fa fa-glass"></span>
            <p>Lorem ipsum dolor sit, consectetur adipisc elit. </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 grids-feature mt-lg-0 mt-4">
          <div class="area-box">
            <h4><a href="#feature" class="title-head">Pub Night</a></h4>
            <span class="fa fa-gift"></span>
            <p>Lorem ipsum dolor sit, consectetur adipisc elit. </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 grids-feature mt-lg-0 mt-4"  data-aos="fade-down">
          <div class="area-box">
            <h4><a href="#feature" class="title-head">Celebrations</a></h4>
            <span class="fa fa-refresh"></span>
            <p>Lorem ipsum dolor sit, consectetur adipisc elit. </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- //bottom-grids-->
  <!--/-->
  <section class="w3l-index3" id="about">
    <div class="midd-w3 py-5">
      <div class="container py-lg-5 py-md-3">
        <div class="row">
          <div class="col-lg-7">
            <div class="row">
              <div class="col-6 position-relative">
                <img src="assets/images/ab1.jpg" alt="" class="radius-image img-fluid">
              </div>
              <div class="col-6 position-relative">
                <img src="assets/images/ab2.jpg" alt="" class="radius-image img-fluid">
              </div>
            </div>
          </div>
          <div class="col-lg-5 mt-lg-0 mt-md-5 mt-4 align-self pl-lg-5">
            <div class="header-title">
              <span class="sub-title">New Year Eve</span>
              <h3 class="title-w3l">Spread the cheer because it is new year!</h3>

            </div>
            <p class="mt-md-4 mt-3">Lorem ipsum viverra feugiat. Pellen tesque libero ut justo,
              ultrices in ligula. Semper at tempufddfel. Lorem ipsum dolor sit amet
              elit. Non quae, fugiat nihil ad. Lorem ipsum dolor sit amet. Lorem ipsum init
              dolor sit.</p>
            <a class="btn btn-primary btn-style mt-md-5 mt-4" href="about.html">Read More</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--//-->
  <!--/grids2-->
  <section class="w3l-how-grids-3" id="how">
    <div class="container-fluid">
      <div class="row d-grid grid-col-2 grid-element-9 px-lg-0">
        <div class="left-texthny p-lg-5 py-4">
          <div class="left-texthny-2 p-lg-5 p-4">
            <h6 class="sub-title"> Upcoming Events</h6>
            <h3 class="title-w3l">Event 1 Party Night
            </h3>
            <p class="my-3 pr-lg-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur hic odio
              voluptatem
              tenetur consequatur.Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit beatae laudantium
              voluptate rem ullam dolore nisi voluptatibus esse quasi.</p>
            <a href="about.html" class="btn btn-style btn-primary mt-4">Read More</a>
          </div>
        </div>
        <!-- <div class="left-grid-ele-9 grid-bg1"> -->
        </div>
      </div>
      </div>
    </div>
  </section>
  <!--//grids2-->
  <!--/pricing-columns-->
  <section class="pricing-columns pricing-section py-5 text-center" id="pricing">
    <div class="container pricing-style-w3ls py-lg-5 py-md-3">
      <div class="header-title mb-4">
        <span class="sub-title">Event Prices</span>
        <h3 class="title-w3l">Get Tickets Now</h3>
      </div>
      <div class="row pricing-chart">
        <div class="col-lg-4 pricing-gd" data-aos="fade-up">
          <div class="plan popular">
            <h3 class="pop-plan">Standard Pass</h3>
            <div class="price mb-4">
              <span class="dollar">$</span>
              <span class="amount" data-dollar-amount="39">39</span>
              <span class="month">/mo</span>
            </div>
            <ul>
              <li>Lorem ipsum dolor </li>
              <li>Consectetur adipisicing</li>
              <li>Lorem ipsum dolor </li>
              <li>Consectetur adipisicing</li>
              <li>Dolor ipsum non velit</li>
            </ul>
            <a class="btn btn-style btn-border mt-lg-5 mt-4" href="#url">Purchase Now</a>
          </div>
        </div>
        <div class="col-lg-4 pricing-gd">
          <div class="plan popular active">
            <h3 class="pop-plan">Gold Pass</h3>
            <div class="price mb-4">
              <span class="dollar">$</span>
              <span class="amount" data-dollar-amount="49">49</span>
              <span class="month">/mo</span>
            </div>
            <ul>
              <li>Lorem ipsum dolor </li>
              <li>Consectetur adipisicing</li>
              <li>Lorem ipsum dolor </li>
              <li>Consectetur adipisicing</li>
              <li>Dolor ipsum non velit</li>
            </ul>
            <a class="btn btn-style btn-primary mt-lg-5 mt-4" href="#url">Purchase Now</a>
          </div>
        </div>
        <div class="col-lg-4 pricing-gd" data-aos="fade-up">
          <div class="plan popular">
            <h3 class="pop-plan">Platinum Pass</h3>
            <div class="price mb-4">
              <span class="dollar">$</span>
              <span class="amount" data-dollar-amount="69">69</span>
              <span class="month">/mo</span>
            </div>
            <ul>
              <li>Lorem ipsum dolor </li>
              <li>Consectetur adipisicing</li>
              <li>Lorem ipsum dolor </li>
              <li>Consectetur adipisicing</li>
              <li>Dolor ipsum non velit</li>
            </ul>
            <a class="btn btn-style btn-border mt-lg-5 mt-4" href="#url">Purchase Now</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--//pricing-columns-->
  <!--/testimonials -->
  <section class="w3l-clients-1" id="testimonials">
    <!-- /grids -->
    <div class="cusrtomer-layout py-5">
      <div class="container py-lg-5 py-md-4">
        <div class="heading text-center mx-0">
          <h6 class="sub-title text-center mx-0">Testimonials</h6>
          <h3 class="title-w3l">
            What our Clients say</h3>
        </div>
        <!-- /grids -->
        <div class="testimonial-row mt-5">
          <div id="owl-demo1" class="owl-two owl-carousel owl-theme mb-md-3 mb-sm-5 mb-4">
            <div class="item">
              <div class="testimonial-content">
                <div class="testimonial">
                  <div class="test-img">
                    <img src="assets/images/team1.jpg" class="img-fluid" alt="client-img">
                  </div>
                  <blockquote>
                    <q>Lorem ipsum dolor sit amet int consectetur adipisicing elit. Velita beatae
                      laudantium Quas minima sunt natus tempore, maiores aliquid modi felis vitae
                      facere aperiam sequi optio lacinia id ipsum non velit, culpa.
                      voluptate rem ullam dolore nisi est quasi, doloribus tempora.</q>
                  </blockquote>
                  <div class="separatorhny"></div>
                  <div class="testi-des">
                    <div class="peopl align-self">
                      <h3>Gloria Parker</h3>
                      <p class="indentity">Example Name </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="testimonial-content">
                <div class="testimonial">
                  <div class="test-img"><img src="assets/images/team2.jpg" class="img-fluid" alt="client-img">
                  </div>
                  <blockquote>
                    <q>Lorem ipsum dolor sit amet int consectetur adipisicing elit. Velita beatae
                      laudantium Quas minima sunt natus tempore, maiores aliquid modi felis vitae
                      facere aperiam sequi optio lacinia id ipsum non velit, culpa.
                      voluptate rem ullam dolore nisi est quasi, doloribus tempora.</q>
                  </blockquote>
                  <div class="separatorhny"></div>
                  <div class="testi-des">
                    <div class="peopl align-self">
                      <h3>Tommy sakura</h3>
                      <p class="indentity">Example Name </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="testimonial-content">
                <div class="testimonial">
                  <div class="test-img"><img src="assets/images/team3.jpg" class="img-fluid" alt="client-img">
                  </div>
                  <blockquote>
                    <q>Lorem ipsum dolor sit amet int consectetur adipisicing elit. Velita beatae
                      laudantium Quas minima sunt natus tempore, maiores aliquid modi felis vitae
                      facere aperiam sequi optio lacinia id ipsum non velit, culpa.
                      voluptate rem ullam dolore nisi est quasi, doloribus tempora.</q>
                  </blockquote>
                  <div class="separatorhny"></div>
                  <div class="testi-des">
                    <div class="peopl align-self">
                      <h3>Bruce Bailey </h3>
                      <p class="indentity">Example Name </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="testimonial-content">
                <div class="testimonial">
                  <div class="test-img"><img src="assets/images/team4.jpg" class="img-fluid" alt="client-img">
                  </div>
                  <blockquote>
                    <q>Lorem ipsum dolor sit amet int consectetur adipisicing elit. Velita beatae
                      laudantium Quas minima sunt natus tempore, maiores aliquid modi felis vitae
                      facere aperiam sequi optio lacinia id ipsum non velit, culpa.
                      voluptate rem ullam dolore nisi est quasi, doloribus tempora.</q>
                  </blockquote>
                  <div class="separatorhny"></div>
                  <div class="testi-des">
                    <div class="peopl align-self">
                      <h3>Ruth Edwards</h3>
                      <p class="indentity">Journalist</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /grids -->
    </div>
    <!-- //grids -->
  </section>
  <!-- //testimonials -->

  <!-- footer7 block -->
  <section class="w3l-footer7">
    <div class="footer7 py-5">
      <div class="container py-lg-3">
        <div class="footer-destinations mb-3 text-center">
          <h5>The life of the party. The sound of celebration.</h5>
        </div>   
        <div class="col-lg-6 col-sm-3 mt-lg-0 mt-sm-0 mt-4 p-md-0">
        <h3 class="heading mb-2"> Newsletter </h3>
          <p class="mb-5">Enregistrez-vous et recevez 15% de reduction</p>
            <form action="#" method="post" style="margin-bottom: 50px;">
              <input class="form-control" type="email" placeholder="Votre adresse Email" name="Subscribe" required="">
                  <button type="submit" class="btn1 btn-primary btn-block btn-flat">SEND</button>
          </div>
              
            </form>
      </div>

    </div>
      <!-- //footer -->
      <!-- move top -->
      <button onclick="topFunction()" id="movetop" title="Go to top">
        <span class="fa fa-angle-up"></span>
      </button>
 </section>
      <script>
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function () {
          scrollFunction()
        };

        function scrollFunction() {
          if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("movetop").style.display = "block";
          } else {
            document.getElementById("movetop").style.display = "none";
          }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
          document.body.scrollTop = 0;
          document.documentElement.scrollTop = 0;
        }
      </script>
      <!-- //move top -->
      <!-- /noscroll -->
      <script>
        $(function () {
          $('.navbar-toggler').click(function () {
            $('body').toggleClass('noscroll');
          })
        });
      </script>
      <!-- //noscroll -->
      <!-- //footer -->
    </div>
  <!-- //footer7 block -->

  <!--/MENU-JS-->
  <script>
    $(window).on("scroll", function () {
      var scroll = $(window).scrollTop();

      if (scroll >= 80) {
        $("#site-header").addClass("nav-fixed");
      } else {
        $("#site-header").removeClass("nav-fixed");
      }
    });

    //Main navigation Active Class Add Remove
    $(".navbar-toggler").on("click", function () {
      $("header").toggleClass("active");
    });
    $(document).on("ready", function () {
      if ($(window).width() > 991) {
        $("header").removeClass("active");
      }
      $(window).on("resize", function () {
        if ($(window).width() > 991) {
          $("header").removeClass("active");
        }
      });
    });
  </script>
  <!--//MENU-JS-->
  <script>
    $('#drop').change(function () {
      if ($('#drop').is(":checked")) {
        $('body').css('overflow', 'hidden');
      } else {
        $('body').css('overflow', 'auto');
      }
    });
  </script>
 