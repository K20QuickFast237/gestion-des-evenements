

<div id="register-popup" class="popup-effect">
  <div class="popup">
    <div class="lregister-form">
      
<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="<?php echo site_url(array('Welcome','index')); ?>"><b>Devenir Membre |  </b>Inscription</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg">Inscrivez vous pour accéder aux fonctionnalités Avancées</p>

      <?php 

        if (isset($_SESSION['message'])) {
          echo('<p class="login-box-msg">'.$_SESSION['message'].'</p>');
        } 

      ?>

      <form action="<?php echo site_url(array('Welcome','manageInscription')) ?>" method="post">
        <div class="form-group has-feedback">
        
          <input type="text" class="form-control" placeholder="Votre Nom*" name="nom" required title="Ce Champ est obligatoire">
          <span class="glyphicon glyphicon-user form-control-feedback position"></span>
      
        </div>
        <div class="form-group has-feedback">
        
          <input type="text" class="form-control" placeholder="Votre Numero De Telephone*" name="telephone" required title="Ce Champ est obligatoire">
          <span class="glyphicon glyphicon-phone form-control-feedback position"></span>
      
        </div>
        <div class="form-group has-feedback">
        
          <input type="email" class="form-control" placeholder="Votre Email*" name="email" required title="Ce Champ est obligatoire">
          <span class="glyphicon glyphicon-envelope form-control-feedback position"></span>
      
        </div>
        <div class="form-group has-feedback">
        
          <input type="password" class="form-control" placeholder="Votre Mot De Passe*" name="password" required title="Ce Champ est obligatoire">
          <span class="glyphicon glyphicon-lock form-control-feedback position"></span>
      
        </div>
        <div class="form-group has-feedback">
        
          <input type="password" class="form-control" placeholder="Confirmez Votre Mot De Passe*" name="password_confirm" required title="Ce Champ est obligatoire">
          <span class="glyphicon glyphicon-lock form-control-feedback position"></span>
      
        </div>
        <div class="form-group has-feedback">
          
          <!-- <label for="Profil">Charger Une Photo De Profil</label> -->
          <span class="glyphicon glyphicon-picture form-control-feedback position"></span>
          <input type="file" class="form-control" name="profil" id="Profil" placeholder="Photo De Profil" accept="image/*" title="Choisir Une Image" onchange="loadFile(event)">
          
        </div>
        <div class="row">
          
          <div class="col-xs-12">
            <img id="img" style="width: 80%;">
          </div>
          <!-- /.col -->
        </div>
        <div class="row">
          
          <div class="col-xs-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat">S'Inscrire</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
  </div>
</body>
