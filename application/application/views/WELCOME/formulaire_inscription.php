<!doctype html>


<!-- popup for register -->
<div id="register-popup" class="popup-effect">
	<div class="popup">
		<div class="lregister-form">
			

<!-- <body class="hold-transition login-page"> -->
  <div class="login-box">
    <div class="login-logo">
    	<h2><center>
      <a href="<?php echo site_url(array('Welcome','index')); ?>"><b>Devenir Membre   </b>Inscription</a></center></h2>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <h4 class="login-box-msg"><center>Inscrivez vous pour accéder aux fonctionnalités Avancées</center></h4><br>

      <?php 

        if (isset($_SESSION['message'])) {
          echo('<p class="login-box-msg">'.$_SESSION['message'].'</p>');
        } 

      ?>

      <form action="<?php echo site_url(array('Welcome','manageInscription')) ?>" method="post">
        <div  class="form-group">
        
          <input type="text" class="form-control" placeholder="Votre Nom*" name="nom" required title="Ce Champ est obligatoire">
          <span class=""></span>
      
        </div>
        <div class="form-group">
        
          <input type="text" class="form-control" placeholder="Votre Numero De Telephone*" name="telephone" required title="Ce Champ est obligatoire">
      
        </div>
        <div class="form-group">
        
          <input type="email" class="form-control" placeholder="Votre Email*" name="email" required title="Ce Champ est obligatoire ">
          <span class=""></span>
      
        </div>
        <div class="form-group">
        
          <input type="password" class="form-control" placeholder="Votre Mot De Passe*" name="password" required title="Ce Champ est obligatoire ">
          <span class=""></span>
      
        </div>
        <div class="form-group">
        
          <input type="password" class="form-control" placeholder="Confirmez Votre Mot De Passe*" name="password_confirm" required title="Ce Champ est obligatoire ">
          <span class=""></span>
      
        </div>
        <div class="form-group">
          
          <!-- <label for="Profil">Charger Une Photo De Profil</label> -->
          <input type="file" class="form-control" name="profil" id="Profil" placeholder="Photo De Profil" accept="image/*" title="Choisir Une Image" onchange="loadFile(event)">

          
        </div>
        <div class="row" style="maxwidth: 15px, maxheight: 5px;">
          
          <div class="col-xs-12"style="width: 5px, height: 5px;">
            <img id="img" >
          </div>
          <!-- /.col -->
        </div>
        <div class="row">
          
          <div class="col-xs-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat">S'Inscrire</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

		</div>
		<a class="close" href="#">&times;</a>
	</div>











