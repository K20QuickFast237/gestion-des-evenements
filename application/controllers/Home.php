<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

		// public $dataUser;
		// public $dataClient;
	
	public function index()
	{
		$this->classifieEvenement(); //fonction qui actualise le statut des evenements (archive ou en cours).
		$data['image'] = $this->Users->findAllUsersBd();
		$data['ActiveEvent']=$this->Event->findActiveEvenementBd();
		$data['AllCategorie']=$this->Categorie->findAllCategorieBd();

		$this->load->view('HOME/header');
		// $this->load->view('HOME/index');
		$this->load->view('HOME/body',$data);
		$this->load->view('HOME/footer');

		$this->load->view('HOME/formulaire_inscription');
		// $this->load->view('HOME/modif_profil');
	}

	public function classifieEvenement(){
		$evenement = $this->Event->findAllEvenementBd();
			// print_r($evenement);
		if ($evenement['data'] == 'ok') {
			$date = date('Y-m-d');

			$today = strtotime($date);

			for ($i=0; $i < $evenement['total']; $i++) { 
				$fin = strtotime($evenement[$i]['date_fin']);
				if ($today > $fin) {
					//on redefini l'etat en tant que archive
					$this->Event->hydrate(array('id'=>$evenement[$i]['id'], 'etat'=>'Archive'));
					$this->Event->actualiseEtat();
				}
			}
		}
	}


}
