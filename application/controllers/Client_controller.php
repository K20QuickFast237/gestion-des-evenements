
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Client_controller extends CI_Controller {

		
		public function index()
		{
			$image['image'] = $this->Users->findAllUsersBd();
			
			$this->load->view('USERS/index');
			$this->load->view('USERS/navigation');
			$this->load->view('USERS/home');
			$this->load->view('ADMIN/footer');
		
		}

		public function creerEvenement()
		{
			$data['categorie'] = $this->Categorie->findAllCategorieBd();

			$this->load->view('WELCOME/header',array('titre'=>'Créer un Evènement'));
			$this->load->view('USERS/index');
			$this->load->view('USERS/navigation');
			$this->load->view('EVENEMENT/AddEvent',$data);
			$this->load->view('ADMIN/footer');
		}

		public function mesevenement(){

			$data['categorie']= $this->Categorie->findAllCategorieBd();
			$this->Event->hydrate(array('id_client'=>$_SESSION['USER']['id']));
			$data['donnees'] = $this->Event->findClientEvenementBd();
			$this->load->view('USERS/index');
			$this->load->view('USERS/navigation');
			$this->load->view('EVENEMENT/listeEvent',$data);
			$this->load->view('ADMIN/footer');
		}


}
