<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

		// private $cible;  //penser a activer la fonction hydrate si vous activez cette variable

		public function index()
		{
			$image['image'] = $this->Users->findAllUsersBd();
			// print_r($image);
			$this->load->view('WELCOME/header');
			$this->load->view('WELCOME/index',$image);
			$this->load->view('WELCOME/formulaire_inscription');
			$this->load->view('WELCOME/footer');
		}


		public function renseigneinscription()  // Charge le formulaire d'inscription
		{
			// $data['lien']=$lien;	
			$this->load->view('Login/header',array('titre'=>'InscriptionUser'));
			$this->load->view('WELCOME/formulaire_inscription');
			$this->load->view('WELCOME/footer');
		}

		public function renseigneconnexion()	// Charge le formulaire de connexion
		{
			// $data['lien']=$lien;
			$this->load->view('WELCOME/header',array('titre'=>'ConnexionUser'));
			$this->load->view('WELCOME/InscriptionUser');
			$this->load->view('WELCOME/footer');
		}
/*
		public function ceerEvenement()

		{
			if (isset($_SESSION['USER'])) {

				$this->load->view('WELCOME/header',array('titre'=>'Créer un Evènement'));
				$this->load->view('EVENEMENT/AddEvent');
				$this->load->view('WELCOME/footer');

			} else {
			   	 session_destroy();
				 redirect(site_url(array('Login','connexionForm')));
			}
			

		}
*/

		/* // premiere version obsolette
		public function manageInscription()
		{	

			if(isset($_SESSION['USER']) || isset($_SESSION['ADMIN'])){
				echo "une session existe";
				print_r($_SESSION);
				redirect(site_url(array('Utilisateur','index')));
				// redirect(site_url(array('Users','DasbordClient')));	  // redirection vers le formulaire de creation d'evenement


			}else{

				if ( !empty($_FILES) AND ($_FILES['profil']['error'] == 0) ){
					echo "<p>fichier defini sans erreur</p>";
                	print_r($_FILES);
					// print_r($_FILES);
					if ($_FILES['profil']['size'] <= 100000000){
						echo "<p>fichier defini a la bonne taille</p>";
						// print_r($_FILES);
                        // Testons si l'extension est autorisée
                        $infosfichier =pathinfo($_FILES['profil']['name']);   // separe les noms et extensions de fichier
                        $extension_upload = $infosfichier['extension'];

                        $config =$infosfichier['filename'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i');
 						$ma_variable = str_replace('.', '_', $config);
						$ma_variable = str_replace(' ', '_', $config);
						$config = $ma_variable.'.'.$extension_upload;
						move_uploaded_file($_FILES['profil']['tmp_name'],'assets/images/user_profil/'.$config);
						$dataUser['profil']=$config;
						echo('<br>data1:');
						print_r($dataUser);
					
                    }else{
						// $this->Welcome->renseigneinscription('');
						echo "<p>fichier defini trop grand</p>";
						$_SESSION['message'] = 'La taille du fichier choisi  est très grande veuillez le remplacer svp !!';
						redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription

                    }
                }else{
                	echo "<p>fichier defini avec erreur</p>";
                	print_r($_FILES);
                	echo "<br>";
                	$_SESSION['message'] = 'L\'image choisie  est endommagée  veuillez la remplacer svp !!';
					redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
					
                }

				if ($_POST!=array()) {
					echo "Post non vide";
					print_r($_POST);
					if($_POST['password']==$_POST['password_confirm']){

						$dataUser['nom'] = $_POST['nom'];
						$_SESSION['USER']['nom'] = $_POST['nom'];

						$dataUser['telephone'] = $_POST['telephone'];
						$_SESSION['USER']['telephone'] = $_POST['telephone'];

						$dataClient['email'] = $_POST['email'];
						$_SESSION['USER']['email'] = $_POST['email'];
						$dataUSer['email'] = $_POST['email'];

						$dataClient['password'] = $_POST['password'];
						$_SESSION['USER']['password'] = $_POST['password'];

						$_SESSION['CLIENT'] = 'ok';

		            	echo "<p>Pas ok!!!</p>";
						print_r($dataClient);
		            	echo "<p>Pas ok!!!</p>";
					}else{
						$_SESSION['message'] = 'La Confirmation Du Mot De Passe Est Incorrecte';
						redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
					
					}


		            if ($this->Client->testExistemail($dataClient['email']) == 'ok') {  //verifie si cet email existe deja
		       			$f = $this->Client->testExistemail($dataClient['email']);
		            	echo ("<p>email deja existant</p>");
		            	print_r($dataClient);
		            	print_r($f);
		            	echo ("<p>email deja existant</p>");
		            	$_SESSION['message'] = 'Cette adresse mail existe deja, Veuillez renseigner une autre adresse';
						redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
		            }

		            $dataUser['email'] = $_POST['email'];
		            $dataUser['niveau'] = 2;

		            if ($this->Users->testExistelephone($dataUser['telephone']) == 'ok') {
		            	echo "<p>Renseignez un autre numero de telephone SVP!!!</p>";
		            	$_SESSION['message'] = 'Ce numero de telephone existe deja, Veuillez renseigner une autre adresse';
						redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
		            }

		            // insertion de l'utilisateur
		            $this->Users->hydrate($dataUser);
		            $this->Users->addUser($dataUser);

		            // Insertion de Client
		            $dataClient['id_users'] = $this->Users->findUsersId($dataUser);
		            // $_SESSION['USER']['id'] = $dataClient['id_users'];
		            $this->Client->hydrate($dataClient);
		            $this->Client->addClient($dataClient);

		            //redirection vers la page de creation d'evenement
		            session_destroy(message);
		            redirect(site_url(array('Utilisateur','index')));

				}else{
					$_SESSION['message'] = 'Une erreur est survenue pendant le transfert des informations';
					redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
				}
			
			}
			
		}
		*/

		public function manageInscriptionVisiteur()
		{	

			if(isset($_SESSION['USER']) || isset($_SESSION['ADMIN'])){
				
				redirect(site_url(array('Home','index'))); // reactualisons la page d'acceuil

			}else{

				if ( !empty($_FILES) AND ($_FILES['profil']['error'] == 0) ){
					echo "<p>fichier defini sans erreur</p>";
                	// print_r($_FILES);
					// print_r($_FILES);
					if ($_FILES['profil']['size'] <= 100000000){
						echo "<p>fichier defini a la bonne taille</p>";
						// print_r($_FILES);
                        // Testons si l'extension est autorisée
                        $infosfichier =pathinfo($_FILES['profil']['name']);   // separe les noms et extensions de fichier
                        $extension_upload = $infosfichier['extension'];

                        $config =$infosfichier['filename'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i');
 						$ma_variable = str_replace('.', '_', $config);
						$ma_variable = str_replace(' ', '_', $config);
						$config = $ma_variable.'.'.$extension_upload;
						move_uploaded_file($_FILES['profil']['tmp_name'],'assets/images/user_profil/'.$config);
						$dataUser['profil']=$config;
						// $_SESSION['USER']['profil'] = $config;
						echo('<br>data1:');
						print_r($dataUser);
					
                    }else{

						$_SESSION['message'] = 'La taille du fichier choisi  est très grande veuillez le remplacer svp !!';
						redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription

                    }
                }else{

                	$_SESSION['message'] = 'L\'image choisie  est endommagée  veuillez la remplacer svp !!';
					redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
					
                }

				if ($_POST!=array()) {
					echo "Post non vide";
					// print_r($_POST);

					// on verifie que le numero de telephone entre n'existe pas deja en bdd
					if ($this->Users->testExistelephone($_POST['telephone']) == 'ok') {
		            	echo "<p>Renseignez un autre numero de telephone SVP!!!</p>";
		            	$_SESSION['message'] = 'Ce numero de telephone existe deja, Essayez plustôt de vous <a href="'.site_url(array('Home','index#register-popup')).' ">connecter</a>';
						redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
		            }

		            // on verifie que l'email entre n'existe pas deja en bdd
		            if ($this->Users->testExistemail($_POST['email']) == 'ok') {  //verifie si cet email existe deja
		            	echo "<br><br>";
		            	print_r($this->Users->testExistemail($dataUser['email']));
		            	echo "<br><br>";
		       			// $f = $this->Client->testExistemail($dataClient['email']);
		            	echo ("<p>email deja existant</p>");
		          //   	print_r($dataClient);
		          //   	print_r($f);
		          //   	echo ("<p>email deja existant</p>");
		            	$_SESSION['message'] = 'Cette adresse mail existe deja, Essayez plustôt de vous <a href="'.site_url(array('Home','index#register-popup')).' ">connecter</a>';
						redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
		            }

		            // on verifie que les passwords on bien ete confirmes et on effectue les traitements
					if($_POST['password']==$_POST['password_confirm']){

						$dataUser['nom'] = $_POST['nom'];
						// $_SESSION['USER']['nom'] = $_POST['nom'];

						$dataUser['telephone'] = $_POST['telephone'];
						// $_SESSION['USER']['telephone'] = $_POST['telephone'];

						$dataVisiteur['email'] = $_POST['email'];
						// $_SESSION['USER']['email'] = $_POST['email'];
						$dataUser['email'] = $_POST['email'];

						$dataVisiteur['password'] = $_POST['password'];
						// $_SESSION['USER']['password'] = $_POST['password'];

						$dataUser['niveau'] = 4;   // visiteur niveau *********************************
						$_SESSION['VISITEUR'] = 'ok';

					}else{
						$_SESSION['message'] = 'La Confirmation Du Mot De Passe Est Incorrecte';
						redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
					
					}

		            // insertion du User
		            $this->Users->hydrate($dataUser);
		            $this->Users->addUser();
		            $id = $this->Users->findUsersId();
		            $_SESSION['USER'] = $this->Users->findUsersInfo($id);

		            // Insertion du Visiteur
		            // $dataVisiteur['id_users'] = $this->Users->findUsersId();
		            $dataVisiteur['id_users'] = $id;



		            // echo "<br>dataVisiteur<br>";
	            	// print_r($dataVisiteur);
	            	// echo "<br>dataUser<br>";
	            	// print_r($dataUser);
	            	// echo "<br><br>";


		            // $_SESSION['USER']['id'] = $dataVisiteur['id_users'];
		            $this->Visiteur->hydrate($dataVisiteur);
		            $this->Visiteur->addVisiteur();

		            //redirection vers la page d'acceuil
		            unset($_SESSION['message']);
		            redirect(site_url(array('Home','index')));

				}else{
					$_SESSION['message'] = 'Une erreur est survenue pendant le transfert des informations';
					redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
				}
			
			}
			
		}
		public function manageInscriptionClient()
		{	

			if(isset($_SESSION['USER']) || isset($_SESSION['ADMIN'])){
				
				redirect(site_url(array('Home','index'))); // reactualisons la page d'acceuil

			}else{

				if ( !empty($_FILES) AND ($_FILES['profil']['error'] == 0) ){
					echo "<p>fichier defini sans erreur</p>";
                	print_r($_FILES);
					// print_r($_FILES);
					if ($_FILES['profil']['size'] <= 100000000){
						echo "<p>fichier defini a la bonne taille</p>";
						// print_r($_FILES);
                        // Testons si l'extension est autorisée
                        $infosfichier =pathinfo($_FILES['profil']['name']);   // separe les noms et extensions de fichier
                        $extension_upload = $infosfichier['extension'];

                        $config =$infosfichier['filename'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i');
 						$ma_variable = str_replace('.', '_', $config);
						$ma_variable = str_replace(' ', '_', $config);
						$config = $ma_variable.'.'.$extension_upload;
						move_uploaded_file($_FILES['profil']['tmp_name'],'assets/images/user_profil/'.$config);
						$dataUser['profil']=$config;
						// $_SESSION['USER']['profil'] = $config;
						echo('<br>data1:');
						print_r($dataUser);
					
                    }else{

						$_SESSION['message'] = 'La taille du fichier choisi  est très grande veuillez le remplacer svp !!';
						redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription

                    }
                }else{

                	$_SESSION['message'] = 'L\'image choisie  est endommagée  veuillez la remplacer svp !!';
					redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
					
                }

				if ($_POST!=array()) {
					echo "Post non vide";
					print_r($_POST);

					// on verifie que le numero de telephone entre n'existe pas deja en bdd
					if ($this->Users->testExistelephone($_POST['telephone']) == 'ok') {
		            	echo "<p>Renseignez un autre numero de telephone SVP!!!</p>";
		            	$_SESSION['message'] = 'Ce numero de telephone existe deja, Essayez plustôt de vous <a href="'.site_url(array('Home','index#register-popup')).' ">connecter</a>';
						redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
		            }

		            // on verifie que l'email entre n'existe pas deja en bdd
		            if ($this->Users->testExistemail($_POST['email']) == 'ok') {  //verifie si cet email existe deja
		            	echo "<br><br>";
		            	print_r($this->Users->testExistemail($dataUser['email']));
		            	echo "<br><br>";
		       			// $f = $this->Client->testExistemail($dataClient['email']);
		            	echo ("<p>email deja existant</p>");
		          //   	print_r($dataClient);
		          //   	print_r($f);
		          //   	echo ("<p>email deja existant</p>");
		            	$_SESSION['message'] = 'Cette adresse mail existe deja, Essayez plustôt de vous <a href="'.site_url(array('Home','index#register-popup')).' ">connecter</a>';
						redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
		            }

		            // on verifie que les passwords on bien ete confirmes et on effectue les traitements
					if($_POST['password']==$_POST['password_confirm']){

						$dataUser['nom'] = $_POST['nom'];
						// $_SESSION['USER']['nom'] = $_POST['nom'];

						$dataUser['telephone'] = $_POST['telephone'];
						// $_SESSION['USER']['telephone'] = $_POST['telephone'];

						$dataClient['email'] = $_POST['email'];
						// $_SESSION['USER']['email'] = $_POST['email'];
						$dataUser['email'] = $_POST['email'];

						$dataClient['password'] = $_POST['password'];
						// $_SESSION['USER']['password'] = $_POST['password'];

						$dataUser['niveau'] = 2;   // Client niveau *********************************
						$_SESSION['CLIENT'] = 'ok';

					}else{
						$_SESSION['message'] = 'La Confirmation Du Mot De Passe Est Incorrecte';
						redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
					
					}

		            // insertion du User
		            $this->Users->hydrate($dataUser);
		            $this->Users->addUser();
		            $id = $this->Users->findUsersId();
		            $_SESSION['USER'] = $this->Users->findUsersInfo($id);

		            // Insertion du Client
		            $dataClient['id_users'] = $id;



		            // echo "<br>dataClient<br>";
	            	// print_r($dataClient);
	            	// echo "<br>dataUser<br>";
	            	// print_r($dataUser);
	            	// echo "<br><br>";


		            // $_SESSION['USER']['id'] = $dataClient['id_users'];
		            $this->Client->hydrate($dataClient);
		            $this->Client->addClient();

		            //redirection vers la page d'acceuil
		            // redirect(site_url(array('Utilisateur','index')));

				}else{
					$_SESSION['message'] = 'Une erreur est survenue pendant le transfert des informations';
					redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
				}
			
			}
			
		}

		public function manageInscriptionParticipant($id_event)
		{	

			if(isset($_SESSION['USER']) || isset($_SESSION['ADMIN'])){
				
				redirect(site_url(array('Home','index'))); // reactualisons la page d'acceuil

			}else{

				if ( !empty($_FILES) AND ($_FILES['profil']['error'] == 0) ){
					echo "<p>fichier defini sans erreur</p>";
                	print_r($_FILES);
					// print_r($_FILES);
					if ($_FILES['profil']['size'] <= 100000000){
						echo "<p>fichier defini a la bonne taille</p>";
						// print_r($_FILES);
                        // Testons si l'extension est autorisée
                        $infosfichier =pathinfo($_FILES['profil']['name']);   // separe les noms et extensions de fichier
                        $extension_upload = $infosfichier['extension'];

                        $config =$infosfichier['filename'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i');
 						$ma_variable = str_replace('.', '_', $config);
						$ma_variable = str_replace(' ', '_', $config);
						$config = $ma_variable.'.'.$extension_upload;
						move_uploaded_file($_FILES['profil']['tmp_name'],'assets/images/user_profil/'.$config);
						$dataUser['profil']=$config;
						// $_SESSION['USER']['profil'] = $config;
						echo('<br>data1:');
						print_r($dataUser);
					
                    }else{

						$_SESSION['message'] = 'La taille du fichier choisi  est très grande veuillez le remplacer svp !!';
						redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription

                    }
                }else{

                	$_SESSION['message'] = 'L\'image choisie  est endommagée  veuillez la remplacer svp !!';
					redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
					
                }

				if ($_POST!=array()) {
					echo "Post non vide";
					print_r($_POST);

					// on verifie que le numero de telephone entre n'existe pas deja en bdd
					if ($this->Users->testExistelephone($_POST['telephone']) == 'ok') {
		            	echo "<p>Renseignez un autre numero de telephone SVP!!!</p>";
		            	$_SESSION['message'] = 'Ce numero de telephone existe deja, Essayez plustôt de vous <a href="'.site_url(array('Home','index#register-popup')).' ">connecter</a>';
						redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
		            }

		            // on verifie que l'email entre n'existe pas deja en bdd
		            if ($this->Users->testExistemail($_POST['email']) == 'ok') {  //verifie si cet email existe deja
		            	echo "<br><br>";
		            	print_r($this->Users->testExistemail($dataUser['email']));
		            	echo "<br><br>";
		       			// $f = $this->Client->testExistemail($dataClient['email']);
		            	echo ("<p>email deja existant</p>");
		          //   	print_r($dataClient);
		          //   	print_r($f);
		          //   	echo ("<p>email deja existant</p>");
		            	$_SESSION['message'] = 'Cette adresse mail existe deja, Essayez plustôt de vous <a href="'.site_url(array('Home','index#register-popup')).' ">connecter</a>';
						redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
		            }

		            // on verifie que les passwords on bien ete confirmes et on effectue les traitements
					if($_POST['password']==$_POST['password_confirm']){

						$dataUser['nom'] = $_POST['nom'];
						// $_SESSION['USER']['nom'] = $_POST['nom'];

						$dataUser['telephone'] = $_POST['telephone'];
						// $_SESSION['USER']['telephone'] = $_POST['telephone'];

						$dataParticipant['email'] = $_POST['email'];
						// $_SESSION['USER']['email'] = $_POST['email'];
						$dataUser['email'] = $_POST['email'];

						$dataParticipant['password'] = $_POST['password'];
						// $_SESSION['USER']['password'] = $_POST['password'];

						$dataUser['niveau'] = 3;   // Participant niveau *********************************
						$_SESSION['PARTICIPANT'] = 'ok';

					}else{
						$_SESSION['message'] = 'La Confirmation Du Mot De Passe Est Incorrecte';
						redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
					
					}

		            // insertion du User
		            $this->Users->hydrate($dataUser);
		            $this->Users->addUser();
		            $id = $this->Users->findUsersId();
		            $_SESSION['USER'] = $this->Users->findUsersInfo($id);
		            // Insertion du Participant
		            $dataParticipant['id_users'] = $id;
		            $dataParticipant['id_event'] = $id_event;



		            // echo "<br>dataParticipant<br>";
	            	// print_r($dataParticipant);
	            	// echo "<br>dataUser<br>";
	            	// print_r($dataUser);
	            	// echo "<br><br>";


		            // $_SESSION['USER']['id'] = $dataParticipant['id_users'];
		            $this->Participant->hydrate($dataParticipant);
		            $this->Participant->addParticipant();

		            //redirection vers la page d'acceuil
		            // redirect(site_url(array('Home','index')));

				}else{
					$_SESSION['message'] = 'Une erreur est survenue pendant le transfert des informations';
					redirect(site_url(array('Home','index#register-popup1'))); //redirection vers le formulaire d'inscription
				}
			
			}
			
		}

		//fonction qui permet de modifier le profile

		public function modifyProfile()
		{	

			if(isset($_SESSION['USER']) || isset($_SESSION['ADMIN']))
			{

				if ( !empty($_FILES) AND ($_FILES['profil']['error'] == 0) ){
					echo "<p>fichier defini sans erreur</p>";
                	print_r($_FILES);
					// print_r($_FILES);
					if ($_FILES['profil']['size'] <= 100000000){
						echo "<p>fichier defini a la bonne taille</p>";
						// print_r($_FILES);
                        // Testons si l'extension est autorisée
                        $infosfichier =pathinfo($_FILES['profil']['name']);   // separe les noms et extensions de fichier
                        $extension_upload = $infosfichier['extension'];

                        $config =$infosfichier['filename'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i');
 						$ma_variable = str_replace('.', '_', $config);
						$ma_variable = str_replace(' ', '_', $config);
						$config = $ma_variable.'.'.$extension_upload;
						move_uploaded_file($_FILES['profil']['tmp_name'],'assets/images/user_profil/'.$config);
						$data['profil']=$config;
						$_SESSION['USER']['profil'] = $config;
						$_SESSION['USER']['nom'] = $_POST['nom'];
						$_SESSION['USER']['telephone'] = $_POST['telephone'];

						$data['nom'] = $_POST['nom'];
						$data['telephone'] = $_POST['telephone'];
						$data['id'] = $_SESSION['USER']['id'];



		            // insertion du User
		            $this->Users->hydrate($data);
		            $this->Users->modifyUser($data);

		             redirect(site_url(array('Home','index')));


					
                    }else{

						$_SESSION['message'] = 'La taille du fichier choisi  est très grande veuillez le remplacer svp !!';
						redirect(site_url(array('Home','#modify-popup'))); //redirection vers le formulaire d'inscription

                    }
                }else{

                	$_SESSION['message'] = 'L\'image choisie  est endommagée  veuillez la remplacer svp !!';
					redirect(site_url(array('Home','#modify-popup'))); //redirection vers le formulaire d'inscription
					
                }

	            }else{
	            	redirect(site_url(array('Home','index')));
	            }
	        }


		/* // Premiere version Obsolette
		public function manageConnexion(){
			
			if ( !isset($_SESSION['USER']) ) {
				if (isset($_POST['email']) && isset($_POST['password'])) {
					echo(' post recu');
					print_r($_POST);

					echo('post bien recu');
					print_r($_POST);
					// on verifie que cela correspend a un participant
					$user = $this->Participant->findAllParticipantBd();

					for ($i=0; $i < $user['total']; $i++) { 
						if ($user[$i]['email'] == $_POST['email'] && $user[$i]['password'] == $_POST['password']) {
							$dataParticipant = $user[$i];
							break;
							
						}
					}

					// si participant, recuperons ses infos d'utilisateur et redirigeons 
					if (isset($dataParticipant)) {
						$_SESSION['USER'] = $this->Users->findUsersInfo($dataParticipant['id_users']);
						// $_SESSION['USER']['id'] = $dataParticipant['id_users'];
						$_SESSION['PARTICIPANT'] = 'ok';
						// echo('redirection dashbord participant');
						redirect(site_url(array('Utilisateur','index'))); // redirection vers le dashbord participant *************************
					
					}else{
						//on verifie que cela correspond a un client
						$user = $this->Client->findAllClientBd();

						for ($i=0; $i < $user['total']; $i++) { 
							if ($user[$i]['email'] == $_POST['email'] && $user[$i]['password'] == $_POST['password']) {
								$dataClient = $user[$i];
								break;
								
							}
						}
					
						if (isset($dataClient)) {
							$_SESSION['USER'] = $this->Users->findUsersInfo($dataClient['id_users']);
							// $_SESSION['USER']['id'] = $dataClient['id_users'];
							$_SESSION['CLIENT'] = 'ok';
							// echo('redirection dashbord client');

						redirect(site_url(array('Utilisateur','index'))); // redirection vers le dashbord participant *************************
						
						}else{
							$_SESSION['message'] = 'Vos informations ne correpondent a aucun participant! Verifiez les ou Inscrivez-vous.';
							redirect(site_url(array('Home','index#register-popup')));
						}

					}

				}else{
					echo('pas de post recu');
				}
					
			}else{ //redirection vers son dashbord
				
				// redirect(site_url(array('Utilisateur','index')));
				redirect(site_url($cible));
			}
			
		} 
		*/

		// Verion Finale Ok et Operationnelle
		public function manageConnexion(){
			
			if ( !isset($_SESSION['CLIENT']) ) {

				if (isset($_POST['email']) && isset($_POST['password'])) {
					
					//on verifie que cela correspond a un client
					$user = $this->Client->findAllClientBd();

					for ($i=0; $i < $user['total']; $i++) { 
						if ($user[$i]['email'] == $_POST['email'] && $user[$i]['password'] == $_POST['password']) {
							$dataClient = $user[$i];
							break;
							
						}
					}

					// si client, recuperons ses infos d'utilisateur et redirigeons 
					if (isset($dataClient)) {
						$_SESSION['USER'] = $this->Users->findUsersInfo($dataClient['id_users']);
						$_SESSION['CLIENT'] = 'ok';

					// redirect(site_url(array('Client_controller','index'))); // redirection vers le dashbord client 
					redirect(site_url(array('Home','index'))); // reactualisons la page d'acceuil
					
					}else{
						//on verifie que cela correspond a un participant
						$user = $this->Participant->findAllParticipantBd();

						for ($i=0; $i < $user['total']; $i++) { 
							if ($user[$i]['email'] == $_POST['email'] && $user[$i]['password'] == $_POST['password']) {
								$dataParticipant = $user[$i];
								break;
								
							}
						}

						// si participant, recuperons ses infos d'utilisateur et redirigeons 
						if (isset($dataParticipant)) {
							$_SESSION['USER'] = $this->Users->findUsersInfo($dataParticipant['id_users']);
							$_SESSION['PARTICIPANT'] = 'ok';

							// redirect(site_url(array('Participant_controller','index'))); // redirection vers le dashbord participant 
							redirect(site_url(array('Home','index'))); // reactualisons la page d'acceuil
						}else{
							//on verifie que cela correspond a un Visiteur
							$user = $this->Visiteur->findAllVisiteurBd();

							for ($i=0; $i < $user['total']; $i++) { 
								if ($user[$i]['email'] == $_POST['email'] && $user[$i]['password'] == $_POST['password']) {
									$dataVisiteur = $user[$i];
									break;
									
								}
							}

							// si Visiteur, recuperons ses infos d'utilisateur et redirigeons 
							if (isset($dataVisiteur)) {
								$_SESSION['USER'] = $this->Users->findUsersInfo($dataVisiteur['id_users']);
								$_SESSION['VISITEUR'] = 'ok';

								// redirect(site_url(array('Visiteur_controller','index'))); // redirection vers le dashbord Visiteur 
								redirect(site_url(array('Home','index'))); // reactualisons la page d'acceuil
							}else{	//il n'est pas enregistre sur notre plate forme
								$_SESSION['message'] = 'Vos informations ne correpondent a aucun Utilisateur! Verifiez les ou Inscrivez-vous.';
								redirect(site_url(array('Home','index#register-popup')));
							}
						}
					
					}

				}else{
					echo('pas de post recu');
				}
					
			}else{ //redirection vers le dashbordClient
				
				redirect(site_url(array('Home','index'))); // reactualisons la page d'acceuil

			}
			
		}

		// connexion pour participer a un evenement
		public function manageParticipation(){
			$ConfirmParticipation = 'Votre participation a ete ajoutee';
			if (isset($_POST['id_event'])) {
				$id_event = $_POST['id_event'];
			
				if (isset($_SESSION['USER'])) { //si connecte, verifions qui est connecte
					if ($_SESSION['CLIENT'] = 'ok' || $_SESSION['PARTICIPANT'] = 'ok') { //si client ou participant, on verifie s'il n'est pas deja inscrit a cet evenemnt
						
						$AllParticipants = $this->participant->findusersEventById($id_event);
						for ($i=0; $i < $AllParticipants['total']; $i++) { 
							if ($_SESSION['USER']['id'] == $AllParticipants['$i']) {
								$existant = 'ok';
								break;
							}
						}
						if (isset($existant)) { //si inscrit on redirige
							$_SESSION['alerte'] = 'Vous participez deja a cet evenement';
							redirect(site_url(array('Home','index')));
						}else{ //si non inscrit on ajoute aux participants
							$dataParticipant['id_event'] = $id_event;
							$dataParticipant['id_users'] = $_SESSION['USER']['id'];
							$dataParticipant['email'] = $_SESSION['USER']['email'];
							$dataParticipant['password'] = $this->Client->findClientpassword($_SESSION['USER']['id']);

							$this->Participant->hydrate($dataParticipant);
							$this->Participant->addParticipant();
						}
					}
					else if ($_SESSION['VISITEUR'] = 'ok') { //si visiteur, on verifie s'il n'est pas deja inscrit a cet evenemnt
						
						//on ajoute aux participants
						$dataParticipant['id_event'] = $id_event;
						$dataParticipant['id_users'] = $_SESSION['USER']['id'];
						$dataParticipant['email'] = $_SESSION['USER']['email'];
						$dataParticipant['password'] = $this->Client->findClientpassword($_SESSION['USER']['id']);

						$this->Participant->hydrate($dataParticipant);
						$this->Participant->addParticipant();

						//on modifie son niveau utilisateur
						$this->Users->hydrate($_SESSION['USER']['email']);
						$this->Users->modifyNiveau(3);

						//on supprime en tant que visiteur
						$this->Visiteur->remove($_SESSION['USER']['email']);
						
					}

					$_SESSION['alerte'] = $ConfirmParticipation;
					redirect(site_url(array('Home','index')));
				}else{

					// si non connecte on redirige vers le formulaire de connexion qui apres le traitement de connexion revoie vers cette fonction ci. ************** complique
					// $_SESSION['message'] = 'Veuillez renseignez Quelques informations afin de pouvoir être enregistré.';
					$_SESSION['alerte'] = 'Connectez-vous afin d\'activer votre participation';
					redirect(site_url(array('Home','index')));
				}

			}else{
				$_SESSION['alerte'] = 'une erreur est survenue, Veuillez choisir a nouveau';
				redirect(site_url(array('Home','index')));
			}

			
		}

		// connexion pour creation d'evenement
		public function ConnexionParticipation(){
			
			if ( !isset($_SESSION['CLIENT']) ) {

				if (isset($_POST['email']) && isset($_POST['password'])) {
					
					//on verifie que cela correspond a un client
					$user = $this->Client->findAllClientBd();

					for ($i=0; $i < $user['total']; $i++) { 
						if ($user[$i]['email'] == $_POST['email'] && $user[$i]['password'] == $_POST['password']) {
							$dataClient = $user[$i];
							break;
							
						}
					}

					// si client, recuperons ses infos d'utilisateur et redirigeons 
					if (isset($dataClient)) {
						$_SESSION['USER'] = $this->Users->findUsersInfo($dataClient['id_users']);
						$_SESSION['CLIENT'] = 'ok';

						redirect(site_url(array('Client_controller','creerEvenement'))); // redirection vers le formulaire de creation d'evenement
					
					
					}else{
						//on verifie que cela correspond a un participant
						$user = $this->Participant->findAllParticipantBd();

						for ($i=0; $i < $user['total']; $i++) { 
							if ($user[$i]['email'] == $_POST['email'] && $user[$i]['password'] == $_POST['password']) {
								$dataParticipant = $user[$i];
								break;
								
							}
						}

						// si participant, recuperons ses infos d'utilisateur et redirigeons 
						if (isset($dataParticipant)) {
							$_SESSION['USER'] = $this->Users->findUsersInfo($dataParticipant['id_users']);
							$_SESSION['PARTICIPANT'] = 'ok';

							redirect(site_url(array('Client_controller','creerEvenement'))); // redirection vers le formulaire de creation d'evenement
							
						}else{
							//on verifie que cela correspond a un Visiteur
							$user = $this->Visiteur->findAllVisiteurBd();

							for ($i=0; $i < $user['total']; $i++) { 
								if ($user[$i]['email'] == $_POST['email'] && $user[$i]['password'] == $_POST['password']) {
									$dataVisiteur = $user[$i];
									break;
									
								}
							}

							// si Visiteur, recuperons ses infos d'utilisateur et redirigeons 
							if (isset($dataVisiteur)) {
								$_SESSION['USER'] = $this->Users->findUsersInfo($dataVisiteur['id_users']);
								$_SESSION['VISITEUR'] = 'ok';

								redirect(site_url(array('Client_controller','creerEvenement'))); // redirection vers le formulaire de creation d'evenement
								
							}else{	//il n'est pas enregistre sur notre plate forme
								$_SESSION['message'] = 'Vos informations ne correpondent a aucun participant! Verifiez les ou Inscrivez-vous.';
								redirect(site_url(array('Home','index#register-popup')));
							}
						}
					
					}

				}else{
					echo('pas de post recu');
				}
					
			}else{ //redirection vers le formulaire de creation d'evenemnt
				
				redirect(site_url(array('Client_controller','creerEvenement'))); // redirection vers le formulaire de creation d'evenement

			}
			
		}

		// connexion pour creation d'evenement
		public function ConnexionEvenement(){
			
			if ( !isset($_SESSION['USER']) ) {  //si ce n'est pas un client

				if (isset($_POST['email']) && isset($_POST['password'])) {
					
					//on verifie que cela correspond a un client
					$user = $this->Client->findAllClientBd();

					for ($i=0; $i < $user['total']; $i++) { 
						if ($user[$i]['email'] == $_POST['email'] && $user[$i]['password'] == $_POST['password']) {
							$dataClient = $user[$i];
							break;
							
						}
					}

					// si client, recuperons ses infos d'utilisateur et redirigeons 
					if (isset($dataClient)) {
						$_SESSION['USER'] = $this->Users->findUsersInfo($dataClient['id_users']);
						$_SESSION['CLIENT'] = 'ok';

						redirect(site_url(array('Client_controller','creerEvenement'))); // redirection vers le formulaire de creation d'evenement
					
					
					}else{
						//on verifie que cela correspond a un participant
						$user = $this->Participant->findAllParticipantBd();

						for ($i=0; $i < $user['total']; $i++) { 
							if ($user[$i]['email'] == $_POST['email'] && $user[$i]['password'] == $_POST['password']) {
								$dataParticipant = $user[$i];
								break;
								
							}
						}

						// si participant, recuperons ses infos d'utilisateur et redirigeons 
						if (isset($dataParticipant)) {
							$_SESSION['USER'] = $this->Users->findUsersInfo($dataParticipant['id_users']);
							$_SESSION['PARTICIPANT'] = 'ok';

							redirect(site_url(array('Client_controller','creerEvenement'))); // redirection vers le formulaire de creation d'evenement
							
						}else{
							//on verifie que cela correspond a un Visiteur
							$user = $this->Visiteur->findAllVisiteurBd();

							for ($i=0; $i < $user['total']; $i++) { 
								if ($user[$i]['email'] == $_POST['email'] && $user[$i]['password'] == $_POST['password']) {
									$dataVisiteur = $user[$i];
									break;
									
								}
							}

							// si Visiteur, recuperons ses infos d'utilisateur et redirigeons 
							if (isset($dataVisiteur)) {
								$_SESSION['USER'] = $this->Users->findUsersInfo($dataVisiteur['id_users']);
								$_SESSION['VISITEUR'] = 'ok';

								redirect(site_url(array('Client_controller','creerEvenement'))); // redirection vers le formulaire de creation d'evenement
								
							}else{	//il n'est pas enregistre sur notre plate forme
								$_SESSION['message'] = 'Vos informations ne correpondent a aucun participant! Verifiez les ou Inscrivez-vous.';
								redirect(site_url(array('Home','index#register-popup')));
							}
						}
					
					}

				}else{
					echo('pas de post recu');
				}
					
			}else{ //redirection vers le formulaire de creation d'evenemnt
				
				redirect(site_url(array('Client_controller','creerEvenement'))); // redirection vers le formulaire de creation d'evenement

			}
			
		}

		public function deconnexion(){
			if (isset($_SESSION['USER'])) {
				session_destroy();
			}
			session_destroy();
			redirect(site_url(array('Home','index')));
	
		}

		/* // Si la variable private est activee, on peut et doit activer ceci ************
		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}
		*/
}

?>
	
	