<footer>
    <div class="container py-5">
        <div class="row footer-gap">
            <div class="col-lg-4 col-sm-6">
                <h3 class="text-capitalize mb-3">Adresse</h3>
                <address class="mb-0">
                    <p class=""><span class="fa fa-map-marker"></span> 24e rue AKWA<br>AKWA</p>
                    <p><span class="fa fa-clock-o"></span> Horaires : 10 a.m to 6 p.m</p>
                    <p><span class="fa fa-phone"></span> +237 698 54 56 12</p>
                    <p><span class="fa fa-envelope-open"></span> <a href="mailto:info@example.com">event@example.com</a></p>
                </address>
            </div>
            
            <div class="col-lg-3 offset-lg-1 col-sm-6 mt-lg-0 mt-sm-5 mt-4 pull-right">
                <h3 class="text-capitalize mb-3" id="foot"> Suivez nous</h3>
                <p class="mb-4">Medias sociaux</p>
                <ul class="social mt-lg-0 mt-3">
                    <li class="mr-1"><a href="#"><span class="fa fa-facebook"></span></a></li>
                    <li class="mr-1"><a href="#"><span class="fa fa-twitter"></span></a></li>
                    <li class="mr-1"><a href="#"><span class="fa fa-google-plus"></span></a></li>
                    <li class=""><a href="#"><span class="fa fa-linkedin"></span></a></li>
                    <li class="mr-1"><a href="#"><span class="fa fa-rss"></span></a></li>
                </ul> <br>

                <h3>A Propos de Nous</h3>
                <p style="color:white;">Jeune entreprise de gestion <br>
                d'evenement de tout genre. <br>
                Nous saurons repondre a tous <br>
                vos besoins a temps . <br>
                Nous sommes situes a AKWA RUE 1521</p>

                

            </div>
            <div class="col-md-3 col-sm-6 sub-two-right mt-5 ">
                <h3>Newsletter</h3>
                <form action="#" class="subscribe mb-3" method="post">
                    <input type="email" name="email" placeholder="Votre adresse Email" required="">
                    <button><span class="fa fa-envelope-o"></span></button>
                </form>
                <p>Entrez votre Email et recevez les dernieres informations et nouveautes.
                </p>
            </div>
            
            
        </div>
    </div>
    <div class="copyright pb-5 text-center">
        <p>© 2020 Organisation. Tous droits reserves | Developpe par <a href="http://www.W3Layouts.com" target="_blank">GESTEVENT.COM</a></p>
    </div>
</footer>
    <?php 
        echo js('Inscription_Connexion');
        echo js('bootstrap.min');
        echo js('owl.carousel');
       
    ?>
     <script>
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function () {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("movetop").style.display = "block";
            } else {
                document.getElementById("movetop").style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>

    <script type="text/javascript">
        $(function(){
            $('.owlcarousel').carousel({interval:(5000)};
        })

                 function scrollToAnyPoint (navItem) {   
         var getAttr;   
         $(navItem).click(function(e){    
           e.preventDefault();     getAttr = $(this).attr('href');   var toSection =
          $(getAttr).offset().top;    
          $("html, body").animate({scrollTop:toSection}, 1000)   });
          } 
          scrollToAnyPoint('ul li a');


      jQuery(document).ready(function() {   var duration = 500;  
           jQuery(window).scroll(function() {    
            if (jQuery(this).scrollTop() > 100) { 
                // Si un défillement de 100 pixels ou plus.       // Ajoute le bouton     
                  jQuery('.haut').fadeIn(duration);     } else {       // Sinon enlève le bouton     
                    jQuery('.haut').fadeOut(duration);     }   });                    jQuery('.haut').click(function(event) {     // Un clic provoque le retour en haut animé.     
                        event.preventDefault();     jQuery('html, body').animate({scrollTop: 0}, duration);     return false;   }) })

    </script>

</body>

</html>