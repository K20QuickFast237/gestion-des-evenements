	<div class="blog-section py-5" style="margin-bottom:85px;">
		<div class="container py-md-5 py-4">
			<div id="newevent" class="waviy text-center mb-md-5 mb-4" style="margin-top: 50px; margin-bottom:40px; background-color: #17a2b8;color: white;font-size: 30px;">

				NOUVEAUX EVENEMENTS
			</div>

			<div class="row">
				<?php for ($i=0; $i <$ActiveEvent['total']; $i++) { ?> 
					<div class="col-lg-4 col-md-4 item" >
						<div class="card">
							<div class="card-header p-0 position-relative">
								<a href="#">
									<img class="card-img-bottom d-block radius-image-full" src="<?php echo img_url('Even_img/'.$ActiveEvent[$i]['image']); ?>"
									alt="Card image cap">
								</a>
							</div>
							<div class="card-body blog-details">
								<span class="label-blue"><?php echo $ActiveEvent[$i]['nom']; ?> </span><br>
								<a href="#" class="blog-desc"><?php echo $ActiveEvent[$i]['description']; ?>
								</a>
								
							</div>
							<div class=" send ser-button mt-4">
								<!-- <form  method="post" action="<?php// echo site_url(array('Home','detail_event')); ?>"> -->
								<form  method="post" action="#register-popup">
									<a href="#register-popup">Participer </a>
									<input type="hidden" name="participer" value="<?php echo $ActiveEvent[$i]['id']; ?>">
									<!-- <input type="hidden" name="participer" value=""> -->
								</form>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div> 
<!-- services -->
<div class="services py-5" id="ser">
	<div class="container py-md-5">
		<!-- <h3 class="heading text-capitalize mb-lg-5 mb-4" id="ser"> Nos Services </h3> -->
		<div class="waviy text-center mb-md-5 mb-4" style="margin-top: 50px; margin-bottom:40px; background-color: #17a2b8;color: white;font-size: 30px;">

			NOS SERVICES
		</div>
		<div class="row">
		<?php for ($i=0; $i <$AllCategorie['total'] ; $i++) { ?>
			<div class="col-lg-4 col-md-6 item">
				<div class="card">
					<div class="card-header p-0 position-relative">
						<a href="#">
							<img class="card-img-bottom d-block radius-image-full" src="<?php echo img_url('cat_img/'.$AllCategorie[$i]['image']); ?>"
							alt="Card image cap" style="height: 289px;">
						</a>
					</div>
					<div class="card-body blog-details">
						<span class="label-blue">Ceremonies du nouvel an </span><br>
						<a href="#" class="blog-desc">Ceremonies du nouvel an
						</a>

					</div>
					<div class="ser-button mt-4">
						<a href="#">Cliquer ici pour plus d'informations</a><br>
					</div>
				</div>
			</div>
		<?php } ?>
		</div>
		<div class="row service-grids">
				<div class="col-md-3 col-sm-6">
					<div class="service-grid1">
						<span class="fa fa-globe"></span>
						<h4 class="my-3">Mariages</h4>
						<p>Le jour le plus important de votre vie organisé et géré de maniere exceptionnelle</p>
					</div>
				</div>
			
				<div class="col-md-3 col-sm-6" >
					<div class="service-grid1">
						<span class="fa fa-book"></span>
						<h4 class="my-3">Soirees</h4>
						<p>Organisez vos soirees endiablees et inoubliables autour de programmes bien organisés .</p>				
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="service-grid1">
						<span class="fa fa-diamond"></span>
						<h4 class="my-3">Conference</h4>
						<p>Vos conferences faites rangees reparties comme vous le souhaitez.</p>

					</div>
				</div>
			
			
				<div class="col-md-3 col-sm-6">
					<div class="service-grid1">
						<span class="fa fa-book"></span>
						<h4 class="my-3">Anniversaire</h4>
						<p>Un jour par an , Un jour exceptionnel.</p>				
					</div>
				</div>
		</div>
	</div>
</div>

	<!-- team -->
	<section class="team-team">
			<div class="container py-md-5">
						<div class="waviy white-text text-center mb-sm-5 mb-4">
							<h3 class="heading text-capitalize mb-lg-5 mb-4">Notre équipe </h3> 
						</div>
						<!-- fireworks effect -->

						<!-- <div class="row team-row"> -->
							<div class="col-md-1  col-sm-3  col-xs-3" style= "width: 130px;">
								<div class="team-member text-center">
									<div class="team-img">
										<img style="border-radius: 50%; width: 150px; height: 150px; border: 7px solid #17a2b8;" src="<?php echo img_url('team1.jpg'); ?>" alt="" class="radius-image">
									</div>
									<a href="#url" class="team-title"style="position: relative;left: 27px;">Massouka Alexandre</a>
									<div class="team-details text-center">
										<div class="socials mt-20"style="position: relative;left: 27px;">
											<a href="#url">
												<span class="fa fa-facebook-f"></span>
											</a>
											<a href="#url">
												<span class="fa fa-twitter"></span>
											</a>
											<a href="#url">
												<span class="fa fa-instagram"></span>
											</a>
										</div>
									</div>
								</div>
							</div>
							<!-- end team member -->
							<div class="col-md-1 col-sm-3 col-xs-3 " style="margin-left: 35px; width: 130px;">
								<div class="team-member text-center">
									<div class="team-img">
										<img style="border-radius: 50%; width: 150px; height: 150px; border: 7px solid #17a2b8;" src="<?php echo img_url('logo.png'); ?>" alt="" class="radius-image">
									</div>
									<a href="#url" class="team-title" style="position: relative;left: 27px;">Tombong Kevin</a>
									<div class="team-details text-center">
										<div class="socials mt-20" style="position: relative;left: 27px;">
											<a href="#url">
												<span class="fa fa-facebook-f"></span>
											</a>
											<a href="#url">
												<span class="fa fa-twitter"></span>
											</a>
											<a href="#url">
												<span class="fa fa-instagram"></span>
											</a>
										</div>
									</div>
								</div>
							</div>
							<!-- end team member -->
							<div class="col-md-1 col-sm-3  col-xs-3" style="margin-left: 35px; width: 130px;">
								<div class="team-member text-center">
									<div class="team-img">
										<img style="border-radius: 50%; width: 150px; height: 150px; border: 7px solid #17a2b8;" src="<?php echo img_url('team3.jpg'); ?>" alt="" class="radius-image">
									</div>
									<a href="#url" class="team-title"style="position: relative;left: 27px;">Ebanda Pascal</a>
									<div class="team-details text-center">
										<div class="socials mt-20"style="position: relative;left: 27px;">
											<a href="#url">
												<span class="fa fa-facebook-f"></span>
											</a>
											<a href="#url">
												<span class="fa fa-twitter"></span>
											</a>
											<a href="#url">
												<span class="fa fa-instagram"></span>
											</a>
										</div>
									</div>
								</div>
							</div>
							<!-- end team member -->
							<div class="col-md-1 col-sm-3 col-xs-3 " style="margin-left: 35px; width: 130px;">
								<div class="team-member text-center">
									<div class="team-img">
										<img style="border-radius: 50%; width: 150px; height: 150px; border: 7px solid #17a2b8;" src="<?php echo img_url('team4.jpg'); ?>" alt="" class="radius-image">
									</div>
									<a href="#url" class="team-title"style="position: relative;left: 27px;">Takodjou Stephane</a>
									<div class="team-details text-center">
										<div class="socials mt-20"style="position: relative;left: 27px;">
											<a href="#url">
												<span class="fa fa-facebook-f"></span>
											</a>
											<a href="#url">
												<span class="fa fa-twitter"></span>
											</a>
											<a href="#url">
												<span class="fa fa-instagram"></span>
											</a>
										</div>
									</div>
								</div>
							</div>
							<!-- end team member -->
							<div class="col-md-1 col-sm-3  col-xs-3" style="margin-left: 35px; width: 130px;">
								<div class="team-member text-center">
									<div class="team-img">
										<img style="border-radius: 50%; width: 150px; height: 150px; border: 7px solid #17a2b8;" src="<?php echo img_url('s2.jpg'); ?>" alt="" class="radius-image">
									</div>
									<a href="#url" class="team-title"style="position: relative;left: 27px;">Ngono Appolinaire</a>
									<div class="team-details text-center">
										<div class="socials mt-20"style="position: relative;left: 27px;">
											<a href="#url">
												<span class="fa fa-facebook-f"></span>
											</a>
											<a href="#url">
												<span class="fa fa-twitter"></span>
											</a>
											<a href="#url">
												<span class="fa fa-instagram"></span>
											</a>
										</div>
									</div>
								</div>
							</div>
							<!-- end team member -->
							<div class="col-md-1 col-sm-3  col-xs-3" style="margin-left: 35px; width: 130px;">
								<div class="team-member last text-center">
									<div class="team-img">
										<img style="border-radius: 50%; width: 150px; height: 150px; border: 7px solid #17a2b8;" src="<?php echo img_url('s6.jpg'); ?>" alt="" class="radius-image">
									</div>
									<a href="#url" class="team-title"style="position: relative;left: 27px;">Elouti Berger</a>
									<div class="team-details text-center">
										<div class="socials mt-20"style="position: relative;left: 27px;">
											<a href="#url">
												<span class="fa fa-facebook-f"></span>
											</a>
											<a href="#url">
												<span class="fa fa-twitter"></span>
											</a>
											<a href="#url">
												<span class="fa fa-instagram"></span>
											</a>
										</div>
									</div>
								</div>
							</div>
							<!-- end team member -->
							<div class="col-md-1 col-sm-3  col-xs-3"style="margin-left: 35px; width: 130px;">
								<div class="team-member last text-center" style="height: 130px;">
									<div class="team-img">
										<img style="border-radius: 50%; width: 150px; height: 150px; border: 7px solid #17a2b8;" src="<?php echo img_url('j.png'); ?>" alt="" class="radius-image">
									</div>
									<a href="#url" style="position: relative;left: 27px;">Fadhil Abouba</a>
									<div class="team-details text-center">
										<div class="socials mt-20"style="position: relative;left: 27px;">
											<a href="#url">
												<span class="fa fa-facebook-f"></span>
											</a>
											<a href="#url">
												<span class="fa fa-twitter"></span>
											</a>
											<a href="#url">
												<span class="fa fa-instagram"></span>
											</a>
										</div>
									</div>
								</div>
							</div>
							<!-- end team member -->
						<!-- </div> -->
					<!-- </div> -->
				<!-- </div> -->
			</div>
		<!-- </div> -->
	</section>

	<!-- //team -->

	<!-- Clients -->
	<section  class="clients-main">
		<div class="wthree-different-dot1 py-5">
			<div class="container py-sm-3">
				<h3 class="heading text-capitalize text-center mb-sm-5 mb-4"> Avis Des Clients </h3>
				<div class="row cli-ent">
					<div class="col-lg-4 col-md-6 item g1">
						<div class="row agile-dish-caption">
							<div class="col-lg-11 text-center mx-auto">
								<h5>Michael Johnson</h5>
								<p class="para-w3-agile"> Tres joli site et belle et tres belle plateforme pour sa gestion d'evenements</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 item g1">
						<div class="row agile-dish-caption">
							<div class="col-lg-11 text-center mx-auto">
								<h5>Mary elizabeth</h5>
								<p class="para-w3-agile"> Tres joli site et belle et tres belle plateforme pour sa gestion d'evenements.</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 item g1">
						<div class="row agile-dish-caption">
							<div class="col-lg-11 text-center mx-auto">
								<h5>Elisa kour</h5>
								<p class="para-w3-agile"> Tres joli site, belle et tres belle plateforme pour sa gestion d'evenements.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> <br>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('.send').click(function(){
			$(this).find('form').submit();
		});

		$('.creerEven').click(function(){
			$(this).parent().find('form').submit();
		});
	})
</script>
