<!doctype html>


<!-- popup for register -->

<div id="modify-popup" class="popup-effect">
  <div class="popup">
    <h5 class="modal-title text-uppercase">Connexion</h5>
    <br>
      <?php 

        if (isset($_SESSION['message'])) {
          echo('<p class="login-box-msg">'.$_SESSION['message'].'</p>');
        } 

      ?>
    <div class="login-form">
      <form action="<?php echo site_url(array('Login','manageConnexion')); ?>" method="post" class="px-3 pt-3 pb-0">
        <div class="form-group">
          <label for="recipient-name" class="col-form-label">Email</label>
          <input type="email" class="form-control" placeholder="" name="email" id="recipient-name" required="">
        </div>
        <div class="form-group">
          <label for="recipient-name1" class="col-form-label">Mot de passe</label>
          <input type="password" class="form-control" placeholder="" name="password" id="recipient-name1" required="">
        </div>
        <div class="right-w3l">
          <input type="submit" class="form-control" value="Se Connecter">
        </div>
        <div >
          <!-- <input type="submit" class="form-control" value="Se Connecter"> -->
          <br><a href="#register-popup1"><b><p  style="color: red;  float:left; ">Pas Encore De Compte? Inscrivez-Vous <u>ICI</u></p></b></a>



        </div>

      </form>

    </div>
    <a class="close" href="#">&times;</a>
  </div>
</div>
 



<div id="register-popup1" class="popup-effect">
	<div class="popup">
		<div class="lregister-form">
			

<!-- <body class="hold-transition login-page"> -->
  <div class="login-box">
    <div class="login-logo">
    	<h2> <center> <a href='#'> <b>Devenir Membre</b> </a> </center> </h2>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <h4 class="login-box-msg"><center>Inscrivez vous pour accéder aux fonctionnalités Avancées</center></h4><br>

      <?php 

        if (isset($_SESSION['message'])) {
          echo('<p class="login-box-msg">'.$_SESSION['message'].'</p>');
        } 

      ?>

      <form action="<?php echo site_url(array('Login','manageInscription')) ?>" method="post" enctype = 'multipart/form-data'>

        <div  class="form-group">
        
          <input type="text" class="form-control" placeholder="Votre Nom*" name="nom" required title="Ce Champ est obligatoire">
          <span class=""></span>
      
        </div>
        <div class="form-group">
        
          <input type="text" class="form-control" placeholder="Votre Numero De Telephone*" name="telephone" required title="Ce Champ est obligatoire">
      
        </div>
        <div class="form-group">
        
          <input type="email" class="form-control" placeholder="Votre Email*" name="email" required title="Ce Champ est obligatoire ">
          <span class=""></span>
      
        </div>
        <div class="form-group">
        
          <input type="password" class="form-control" placeholder="Votre Mot De Passe*" name="password" required title="Ce Champ est obligatoire ">
          <span class=""></span>
      
        </div>
        <div class="form-group">
        
          <input type="password" class="form-control" placeholder="Confirmez Votre Mot De Passe*" name="password_confirm" required title="Ce Champ est obligatoire ">
          <span class=""></span>
      
        </div>
        <div class="form-group">
          
          <!-- <label for="Profil">Charger Une Photo De Profil</label> -->
          <input type="file" class="form-control" name="profil" id="Profil" placeholder="Photo De Profil" accept="image/*" title="Choisir Une Image" onchange="loadFile(event)">

          
        </div>
        <div class="row" style="maxwidth: 15px, maxheight: 5px;">
          
          <div class="col-xs-12"style="width: 5px, height: 5px;">
            <img id="img" >
          </div>
          <!-- /.col -->
        </div>
        <div class="row">
          
          <div class="col-xs-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat">S'Inscrire</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

		</div>
		<a class="close" href="#">&times;</a>
	</div>











