<!doctype html>
  <div id="register-popup" class="popup-effect">
    <div class="popup">
      <h5 class="modal-title text-uppercase">Connexion</h5>

      <?php 

      if (isset($_SESSION['message'])) {
        echo('<p class="login-box-msg">'.$_SESSION['message'].'</p>');
      } 

      ?>
      <div class="login-form">
        <?php 
        if (isset($_POST['creerEven'])) { print_r($_POST['creerEven']);
        ?>
        <form action="<?php echo site_url(array('Login','ConnexionEvenement')); ?>" method="post" class="px-3 pt-3 pb-0">
          <?php
        }else{
          ?>
          <form action="<?php echo site_url(array('Login','manageConnexion')); ?>" method="post" class="px-3 pt-3 pb-0">
            <?php
          }
          ?>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Email</label>
            <input type="email" class="form-control" placeholder="" name="email" id="recipient-name" required="">
          </div>
          <div class="form-group">
            <label for="recipient-name1" class="col-form-label">Mot de passe</label>
            <input type="password" class="form-control" placeholder="" name="password" id="recipient-name1" required="">
          </div>
          <div class="right-w3l">
            <input type="submit" class="form-control" value="Se Connecter">
          </div>
          <div >
            <!-- <input type="submit" class="form-control" value="Se Connecter"> -->
            <br><a href="#register-popup1"><b><p  style="color: red;  float:left; ">Pas Encore De Compte? Inscrivez-Vous <u>ICI</u></p></b></a>

          </div>

        </form>

      </div>
      <a class="close" href="#">&times;</a>
    </div>
  </div>

  <?php if (isset($_SESSION['USER'])) { ?>
  <div id="modify-popup" class="popup-effect">
    <div class="popup">
      <h5 class="modal-title text-uppercase">mon profil</h5>
          <?php 

            if (isset($_SESSION['message'])) {
              echo('<p class="login-box-msg">'.$_SESSION['message'].'</p>');
            }
          ?>
      <form action="<?php echo site_url(array('Login','modifyProfile')); ?>" method="post" enctype="multipart/form-data" class="px-3 pt-3 pb-0">
        <div class="form-group">
          <label for="recipient-name" class="col-form-label">Nom</label>
          <input type="text" class="form-control" value="<?php echo $_SESSION['USER']['nom']; ?>"  name="nom" id="recipient-name" required="">
        </div>
        <div class="form-group">
          <label for="recipient-name" class="col-form-label">Telephone</label>
          <input type="tel" class="form-control" value="<?php echo $_SESSION['USER']['telephone']; ?>"  name="telephone" id="recipient-name" required="">
        </div>
        
        <div class="form-group">
          <input type="file" class="form-control" name="profil" id="Profil" placeholder="Photo De Profil" accept="image/*" title="Choisir Une Image" onchange="loadFile(event)">
        </div>
        <div class="col-xs-12">
            <img id="img" style="width:100px; height: 100px; margin-bottom: 20px;" src="<?php echo img_url('user_profil/'.$_SESSION['USER']['profil']) ?>">
          </div>
        <div class="right-w3l">
          <input type="submit" class="form-control" value="Modifier">
        </div>
      </form>

      <a class="close" href="#">&times;</a>
    </div>
  </div>
  <?php } ?>



  <div id="register-popup1" class="popup-effect">
   <div class="popup">
    <div class="lregister-form">
      <div class="login-box">
        <div class="login-logo">
         <h2> <center> <a href='#'> <b>Devenir Membre</b> </a> </center> </h2>
       </div>
       <!-- /.login-logo -->
       <div class="login-box-body">
        <h4 class="login-box-msg"><center>Inscrivez vous pour accéder aux fonctionnalités Avancées</center></h4><br>

        <?php 

        if (isset($_SESSION['message'])) {
          echo('<p class="login-box-msg">'.$_SESSION['message'].'</p>');
        } 

        ?>

        <?php 
        if (isset($_POST['creerEven'])) { print_r($_POST['creerEven']);
        ?>
        <form action="<?php echo site_url(array('Login','manageInscriptionClient')); ?>" method="post" enctype = 'multipart/form-data'>
          <?php
        }else{
          ?>
          <form action="<?php echo site_url(array('Login','manageInscriptionVisiteur')) ?>" method="post" enctype = 'multipart/form-data'>          
            <?php
          }
          ?>

          <div  class="form-group">

            <input type="text" class="form-control" placeholder="Votre Nom*" name="nom" required title="Ce Champ est obligatoire">
            <span class=""></span>

          </div>
          <div class="form-group">

            <input type="text" class="form-control" placeholder="Votre Numero De Telephone*" name="telephone" required title="Ce Champ est obligatoire">

          </div>
          <div class="form-group">

            <input type="email" class="form-control" placeholder="Votre Email*" name="email" required title="Ce Champ est obligatoire ">
            <span class=""></span>

          </div>
          <div class="form-group">

            <input type="password" class="form-control" placeholder="Votre Mot De Passe*" name="password" required title="Ce Champ est obligatoire ">
            <span class=""></span>

          </div>
          <div class="form-group">

            <input type="password" class="form-control" placeholder="Confirmez Votre Mot De Passe*" name="password_confirm" required title="Ce Champ est obligatoire ">
            <span class=""></span>

          </div>
          <div class="form-group">

            <!-- <label for="Profil">Charger Une Photo De Profil</label> -->

            <div class="form-group">
              <input type="file" class="form-control" name="profil" id="Profil" placeholder="Photo De Profil" accept="image/*" title="Choisir Une Image" onchange="loadFile(event)">
            </div>
            <div class="col-xs-12">
                <img id="img"  style="width:100px; height: 100px; margin-bottom: 20px;" >
              </div>
              <!-- /.col -->
            
            <div class="row">

              <div class="col-xs-12">
                <button type="submit" class="btn btn-primary btn-block btn-flat">S'Inscrire</button>
              </div>
              <!-- /.col -->
            </div>
          </form>

        </div>
        <a class="close" href="#">&times;</a>
      </div>











