    
<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">NAVIGATION</li>
    <?php if (isset($session['participant'])) {?>
      <li class="treeview  menu-open  ">
        <a href="<?php echo site_url(array('Client_controller','mesinvitation'));?>">
          <i class="fa fa-th-large text-blue"></i>
          <span>Créer un Evènement</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
      </li>
      <li class="treeview  menu-open  ">
        <a href="<?php echo site_url(array('Client_controller','mesinvitation')); ?>">
          <i class="fa fa-th-large text-blue"></i>
          <span>Mes invitations</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
      </li>
      <li class="treeview  menu-open  ">
        <a href="<?php echo site_url(array('Client_controller','mesevenement')); ?>">
          <i class="fa fa-th-large text-blue"></i>
          <span>Participation</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
      </li>
     <?php }else{?>
      <li>
        <a href="<?php echo site_url(array('Client_controller','mesevenement')); ?>"><i class="fa fa-renren text-red"></i> <span>Home</span></a>
      </li>

      <li class="treeview  menu-open">
        <a href="#">
          <i class="fa fa-users text-green"></i>
          <span>Evènements</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url(array('Client_controller','creerEvenement')); ?>"><i class="fa fa-users"></i>Créer un Evènement</a></li>
          <li><a href="<?php echo site_url(array('Client_controller','mesevenement')); ?>"><i class="fa fa-users"></i>Mes Evènements</a></li>
        </ul>
      </li>

      <li class="treeview  menu-open  ">
        <a href="<?php echo site_url(array('Client_controller','')); ?>">
          <i class="fa fa-th-large text-blue"></i>
          <span>Participation</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
      </li> 
     <?php } ?>
    </ul>
  </section>
</aside>

  
