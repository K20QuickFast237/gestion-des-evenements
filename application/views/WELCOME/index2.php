<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
		<link rel="stylesheet" type="text/css" href="<?php echo css('ionicons.min') ?>">
				<?php 
				echo css('bootstrap.min');
				echo css('event');
				echo css('font-awesome');
				echo css('ionicons.min');
			 ?>

	<?php 
		echo js('jquery-3.6.0.min');
		// echo js('bootstrap.min');
	 ?>

</head>

<body>
    <header>
        <div class="container">
        	<!-- top header -->
        	<section class="row top_header pt-3">
        		<div class="col-lg-6 buttons ml-auto" >
        			<p style="margin-right: 20%;"><span class="fa fa-phone"></span> +237 690 9814 63</p>
        			<a  class="btn btn-info btn-lg-block w3ls-btn px-sm-4 px-3 text-capitalize mr-sm-2" href="#login-popup" style="margin-left: 10px;" >Connexion</a>
        			<a class="btn btn-info1 btn-lg-block w3ls-btn1 px-sm-4 px-3 text-capitalize" href="#register-popup">Inscription</a>
        		</div>
        	</section>
        	<!-- top header -->
        	<!-- nav -->
        	<nav class="py-3">
        		<div id="logo">
        			<h1>
        				<a class="navbar-brand" href="index.html"> <span class="fa fa-empire"></span>GESTION<span><span class="line"></span>EVENEMENTS</span>
        				</a>
        			</h1>
        		</div>

        		<label for="drop" class="toggle"><span class="fa fa-bars"></span></label>
        		<input type="checkbox" id="drop" />
        		<ul class="menu mt-2">
        			<li class="mr-lg-3 mr-2 active"><a href="index.html">Accueil</a></li>
        			<li class="mr-lg-3 mr-2"><a href="about.html">A Propos </a></li>
        			<li class="mr-lg-3 mr-2"><a href="services.html">Services</a></li>
        			<li class="mr-lg-3 mr-2"><a href="gallery.html">Galeries</a></li>
        			<li class="mr-lg-3 mr-2 pb-0">
        				<!-- First Tier Drop Down -->
        				<label for="drop-2" class="toggle">Pages <span class="fa fa-angle-down" aria-hidden="true"></span> </label>
        				<a href="#">Pages <span class="fa fa-angle-down" aria-hidden="true"></span></a>
        				<input type="checkbox" id="drop-2"/>
        				<ul class="drop-down-ul">
        					<li><a href="error.html">ARCHIVES</a></li>
        					<li><a href="events.html">Catalogue</a></li>
        				</ul>
        			</li>
        			<li><a href="contact.html">Contact</a></li>
        		</ul>
        	</nav>
        	<!-- //nav -->
        </div>
    </header>

    <section class="banner layer" id="home">
		<div class="container">
		<div class="banner-text">
			<div class="slider-info mb-4">
				<div class="banner-heading">
					<h3>
						UNE NOUVELLE VISION D'ORGANISATION EVENEMENTIELLE
					</h3>
				</div>
				<a href="contact.html"> Plannifiez Vos Evenements</a>
			</div>
			<!-- To bottom button-->
			<div class="thim-click-to-bottom">
				<div class="rotate">
					<a href="#welcome" class="scroll">
						<span class="fa fa-angle-double-down"></span>
					</a>
				</div>
			</div>
			<!-- //To bottom button-->

		</div>
		</div>
	</section>
								<!-- //banner -->

						<!-- IMAGES -->
	<section class="images" style="margin-bottom: 55px;">
		<div class="container-fluid">
		<div class="row">
			<div class="col-lg-2 col-md-4 col-sm-6  bg-img1">
				<h4><a href="">Plannification</a></h4>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-6 bg-img2">
				<h4><a href="">Mariage</h4></a>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-6 bg-img3">
				<h4>Conference</h4>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-6 bg-img4">
				<h4>Soirees</h4>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-6 bg-img5">
				<h4>Anniversaire</h4>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-6  bg-img1">
				<h4><a href="">Plannification</a></h4>
			</div>
		</div>
		</div>
	</section>
	<!-- IMAGES -->

	<!-- subscribe -->
	<section class="subscribe text-center bg-light py-5">
		<div class="container p-sm-3">
		<h3 class="heading mb-2" style="margin-bottom: 10px;">  Newsletter </h3>
		<p class="mb-5">Enregistrez-vous et recevez 15% de reduction.</p>
		<form action="#" method="post" style="margin-bottom: 50px;">
			<input class="form-control" type="email" placeholder="Votre adresse Email" name="Subscribe" required="">
			<button class="btn1">
				<span class="fa fa-paper-plane"></span>
			</button>
		</form>
		</div>
	</section>
	<!-- //subscribe -->

	<!-- footer -->	
	<footer>
		<div class="container py-5">
		<div class="row footer-gap">
			<div class="col-lg-4 col-sm-6">
				<h3 class="text-capitalize mb-3">Adresse</h3>
				<address class="mb-0">
					<p class=""><span class="fa fa-map-marker"></span> 24e rue AKWA<br>AKWA</p>
					<p><span class="fa fa-clock-o"></span> Horaires : 10 a.m to 6 p.m</p>
					<p><span class="fa fa-phone"></span> +237 698 54 56 12</p>
					<p><span class="fa fa-envelope-open"></span> <a href="mailto:info@example.com">event@example.com</a></p>
				</address>
			</div>
				<div class="col-lg-3 offset-lg-1 col-sm-6 mt-lg-0 mt-sm-5 mt-4">
				<h3 class="text-capitalize mb-3"> Suivez nous</h3>
				<p class="mb-4">Medias sociaux</p>
				<ul class="social mt-lg-0 mt-3">
					<li class="mr-1"><a href="#"><span class="fa fa-facebook"></span></a></li>
					<li class="mr-1"><a href="#"><span class="fa fa-twitter"></span></a></li>
					<li class="mr-1"><a href="#"><span class="fa fa-google-plus"></span></a></li>
					<li class=""><a href="#"><span class="fa fa-linkedin"></span></a></li>
					<li class="mr-1"><a href="#"><span class="fa fa-rss"></span></a></li>
				</ul>
			</div>
		</div>
		</div>
		<div class="copyright pb-5 text-center">
				<p>© 2020 Organisation. Tous droits reserves | Developpe par <a href="http://www.W3Layouts.com" target="_blank">GESTEVENT.COM</a></		p>
		</div>
	</footer>
			<!-- footer -->

			<!-- popup for login -->
	<div id="login-popup" class="popup-effect">
	<div class="popup">
		<h5 class="modal-title text-uppercase">Connexion</h5>
		<div class="login-form">
			<form action="#" method="post" class="px-3 pt-3 pb-0">
				<div class="form-group">
					<label for="recipient-name" class="col-form-label">Utilisateur</label>
					<input type="email" class="form-control" placeholder="" name="Name" id="recipient-name" required="">
				</div>
				<div class="form-group">
					<label for="recipient-name1" class="col-form-label">Mot de passe</label>
					<input type="password" class="form-control" placeholder="" name="Name" id="recipient-name1" required="">
				</div>
				<div class="right-w3l">
					<input type="submit" class="form-control" value="Se Connecter">
				</div>
			</form>

		</div>
		<a class="close" href="#">&times;</a>
	</div>
	</div>
	<!-- popup for login -->

	<!-- popup for register -->
	<div id="register-popup" class="popup-effect">
	<div class="popup">
		<h5 class="modal-title text-uppercase">Inscription</h5>
		<div class="lregister-form">
			<form action="#" method="post" class="px-3 pt-3 pb-0">
				<div class="form-group">
					<label for="recipient-name" class="col-form-label">NOM</label>
					<input type="text" class="form-control" placeholder="" name="Name" id="recipient-name2" required="">
				</div>
				<div class="form-group">
					<label for="recipient-name" class="col-form-label">PRENOM</label>
					<input type="text" class="form-control" placeholder="" name="Name" id="recipient-name3" required="">
				</div>
				<div class="form-group">
					<label for="recipient-name" class="col-form-label">Email </label>
					<input type="email" class="form-control" placeholder="" name="Name" id="recipient-name4" required="">
				</div>
				<div class="form-group">
					<label for="recipient-name" class="col-form-label">Numero</label>
					<input type="text" class="form-control" placeholder="" name="Name" id="recipient-name5" required="">
				</div>
				<div class="form-group">
					<label for="recipient-name1" class="col-form-label">Mot de passe</label>
					<input type="password" class="form-control" placeholder="" name="Name" id="recipient-name6" required="">
				</div>
				<div class="right-w3l">
					<input type="submit" class="form-control" value="Get Started">
				</div>
			</form>
		</div>
		<a class="close" href="#">&times;</a>
	</div>
	</div>
	<!-- popup for register -->

</body>
</html>
