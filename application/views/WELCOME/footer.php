
    <?php 
        echo js('Inscription_Connexion');
        echo js('bootstrap.min');
        echo js('owl.carousel');
       
    ?>
     <script>
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function () {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("movetop").style.display = "block";
            } else {
                document.getElementById("movetop").style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>

    <script type="text/javascript">
        $(function(){
            $('.carousel').carousel({interval:(5000)};
        })

                 function scrollToAnyPoint (navItem) {   
         var getAttr;   
         $(navItem).click(function(e){    
           e.preventDefault();     getAttr = $(this).attr('href');   var toSection =
          $(getAttr).offset().top;    
          $("html, body").animate({scrollTop:toSection}, 1000)   });
          } 
          scrollToAnyPoint('ul li a');


      jQuery(document).ready(function() {   var duration = 500;  
           jQuery(window).scroll(function() {    
            if (jQuery(this).scrollTop() > 100) { 
                // Si un défillement de 100 pixels ou plus.       // Ajoute le bouton     
                  jQuery('.haut').fadeIn(duration);     } else {       // Sinon enlève le bouton     
                    jQuery('.haut').fadeOut(duration);     }   });                    jQuery('.haut').click(function(event) {     // Un clic provoque le retour en haut animé.     
                        event.preventDefault();     jQuery('html, body').animate({scrollTop: 0}, duration);     return false;   }) })

    </script>

</body>

</html>