  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        GESTION CATEGORIE
        <small>CONTROL PANEL</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> LISTE CATEGORIE</a></li>
         
      </ol>

      
    </section>

    <!-- Main content -->

  <section class="content w3l-mag-main">
  <!--/mag-content-->
    <div class="mag-content-inf py-5">
      <div class="container">
          <div class="blog-inner-grids py-md-4 row">

        <?php 
          if ($allCategorie['data']=='ok') {
            for ( $i=0 ; $i < $allCategorie['total'] ; $i++ ) { ?>

        <div class="maghny-grids-inf row">
          <div class="maghny-gd-1 col-lg-4 col-md-6">
            <div class="maghny-grid">
              <figure class="effect-lily">
                <img class="img-fluid" style="height: 278px;" src="<?php echo img_url('cat_img/'.$allCategorie[$i]['image']) ?>" alt="">
                <figcaption>
                  <div>
                    <h2 class="top-text"><?php echo $allCategorie[$i]['nom']; ?></h2>
                  </div>
                </figcaption>
              </figure>
            </div>
            <a href="#" class="read-more btn mt-3"> <?php echo $allCategorie[$i]['description']; ?></a><br>
          </div>
        </div>

      <?php }} ?>
  <!--//mag-content-->
  </section>
</div>
    
  </div>
  

