
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <section class="content-header">
  <h1>
    <font style="vertical-align: inherit;">
    Ajouter une Catégorie
    </font>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="#">
        <i class="fa fa-dashboard"></i>
        <font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Home</font></font>
      </a>
    </li>
    <li>
      <a href="#">
        <i class="fa fa-dashboard"></i>
        <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gestion de Catégories </font></font>
      </a>
    </li>
    <li>
      <a href="#">
        <i class="fa fa-dashboard"></i>
        <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ajouter une Catégorie </font></font>
      </a>
    </li>
  </ol>
  </section>

    <!-- Main content -->
  <section class="content">
  <div class="row">
    <div class=" col-md-offset-3 col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Création d'une nouvelle Catégorie d'évènement</font></font></h3>
        </div>
        <!-- /.box-header -->

        <!-- form start -->
      <form role="form" enctype="multipart/form-data" method="post" action="<?php echo site_url(array('Administration','newCategorie')); ?>">
          <div class="box-body">
            <div class="form-group">
              <label for="InputName_cat">
                <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nom de la Catégorie</font></font>
              </label>
              <input type="text" class="form-control" name="nom" placeholder="Entrez le nom de la Catégorie">
            </div>
            <div class="form-group">
              <label for="TextareaDescription">
                <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Description</font></font>
              </label>
              <textarea cols="15" class="form-control" name="description"></textarea>
            </div>
            <div class="form-group">
              <label for="InputImgCat">
                <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Image de la Catégorie</font></font>
              </label>
              <input type="file" name="image" onchange="loadFile(event)">
              <p class="help-block"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Entrez l'image décrivant votre catégorie.</font></font></p>
            </div>
          </div>
          <!-- /.box-body -->

          <div style="width: 100px; height: 100px;">
            <img style="width: 100px; height: 100px;" id="img">
          </div>

          <div class="box-footer">
            <button type="submit" class="btn btn-primary"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Soumettre</font></font></button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
  

  <script>
  
  var loadFile = function(event) {
    var profil = document.getElementById('img');
    profil.src = URL.createObjectURL(event.target.files[0]);
  };



</script>













































