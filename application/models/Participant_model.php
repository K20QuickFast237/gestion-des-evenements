<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Participant_model extends CI_Model{
		
		function __construct()
			{
			
			}
		
			// gerer un participant

			private $id;
			private $id_users;
			private $id_event;
			private $email;
			private $password;

			protected $table= 'participant';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}



			public function addParticipant(){

			    // $this->db->set('id', $this->id)
			    $this->db->set('id_users', $this->id_users)
			    	->set('email', $this->email)
			    	->set('password', $this->password)
			    	->set('id_event', $this->id_event)
					->insert($this->table);
		
			}

			// fonction qui reccupère juste le mot de passe d'un participant quelconque
			
			public function findPassword($email){
				$data =$this->db->select('password')
								->from($this->table)
								->where('email', $email)
								->limit(1)
								->get()
								->result();

				foreach ($data as $row){
			       	$donnees['password']=$row->password;
				}

				return $donnees['password'];
			}

			// fonction qui charge tous les Participant pour faire le filtrage de donnee
			
			public function findAllParticipantBd(){
				$data = $this->db->select('id,id_users,email,password,id_event')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_users']=$row->id_users;
			       	$donnees[$i]['email']=$row->email;
			       	$donnees[$i]['password']=$row->password;
			       	$donnees[$i]['id_event']=$row->id_event;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}

			// fonction qui reccupère juste l'email d'un Participant

			public function findParticipantemail($id){
				$data =$this->db->select('email')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

								
				foreach ($data as $row){
			       	$donnees['email']=$row->email;
				}

				return $donnees['email'];
			}

			// fonction qui reccupère juste le password d'un Participant

			public function findParticipantpassword($id){
				$data =$this->db->select('password')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

					$donnees['password']='non';				
				foreach ($data as $row){
			       	$donnees['password']=$row->password;
				}

				return $donnees['password'];
			}
			

		    // fonction qui reccupère juste l identifiant utilisateur d'un Participant quelconque
			
			public function findIdUsers($id){
				$data =$this->db->select('id_users')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

				foreach ($data as $row){
			       	$donnees['id_users']=$row->id_users;
				}

				return $donnees['id_users'];
			}

		    // fonction qui reccupère tous les id_users d'un evenement
			
			public function findusersEventById($id_event){
				$data =$this->db->select('id_users')
								->from($this->table)
								->where('id_event', $id_event)
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;
			}

			// fonction qui reccupère juste  l identifiant d un evenement quelconque d'un Participant
			
			public function findIdEvent($id){
				$data =$this->db->select('id_event')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();
						
				foreach ($data as $row){
			       	$donnees['id_event']=$row->id_event;
				}

				return $donnees['id_event'];
			}


			// fonction qui reccupère juste le password et email d'un Participant

			public function findParticipantInfos($email,$password){
				$data =$this->db->select('password,email')
						->from($this->table)
						->where(array('password'=>$password,'email'=>$email))
						->limit(1)
						->get()
						->result();

				$donnees['data']='non';			
				foreach ($data as $row){
			       	$donnees['email']=$row->email;
			       	$donnees['password']=$row->password;
			       	$donnees['data']='ok';
				}

				return $donnees;
			}

			// fonction libere l'espace d'un participant ayant change de statut
			public function moveto($new){

				$this->db->set('email', $new)
				    	 ->set('password', $new)
				    	 ->where('email', $this->email)
						 ->update($this->table);
		
			}

			// fonction libere l'espace d'un participant ayant change de statut
			// public function remove($email){  

			// 	$this->db->where('email', $email)
			// 			 ->delete($this->table);
						 
			// }



			// setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setIdUsers($id_users){
				$this->id_users=$id_users;
			}
			
			public function setEmail($email){
				$this->email=$email;
			}

			public function setPassword($password){
				$this->password=$password;
			}
			public function setIdEvent($id_event){
				$this->id_event=$id_event;
			}
			

			// getteurs

			public function getId(){
				return $this->id;
			
			}

			
			public function getIdUsers(){
				return $this->id_users;
			
			}

			public function getEmail(){
				return $this->email;
			
			}

			public function getPassword(){
				return $this->password;
			
			}
			public function getIdEvent(){
				return $this->id_event;
			
			}
						
	
}

?>
