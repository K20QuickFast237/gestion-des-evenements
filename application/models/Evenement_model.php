<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Evenement_model extends CI_Model{
		
		function __construct()
			{
			
			}
		
			// gerer un evenement

			private $id;
			private $id_categorie;
			private $id_client;
			private $nom;
			private $description;
			private $image;
			private $nb_participant;
			private $date_debut;
			private $date_fin;
			private $heure_debut;
			private $heure_fin;
			private $lieu;
			private $etat;

			protected $table= 'evenement';

		// hydratation, c'est une fonction de filtrage necessaire avant la manipulation des donnees

			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

		// retourne le nombre d'elements d'une liste passee en parametre

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}


		//  Ajoute un evenement
			public function addEvenement(){

			    // $this->db->set('id', $this->id)
				$this->db->set('id_categorie', $this->id_categorie)
				    	 ->set('id_client', $this->id_client)
				    	 ->set('nom', $this->nom)
				    	 ->set('description', $this->description)
				    	 ->set('image', $this->image)
				    	 ->set('nb_participant', $this->nb_participant)
				    	 ->set('date_debut', $this->date_debut)
				    	 ->set('date_fin', $this->date_fin)
				    	 ->set('heure_debut', $this->heure_debut)
				    	 ->set('heure_fin', $this->heure_fin)
				    	 ->set('lieu', $this->lieu)
				    	 ->set('etat', $this->etat)
						 ->insert($this->table);
		
			}


			// fonction qui charge tous les evenements pour faire le filtrage de donnee
			
			public function findAllEvenementBd(){
				$data = $this->db->select('id,id_categorie,id_client,nom,description,image,nb_participant,date_debut,date_fin,heure_debut,heure_fin,lieu,etat')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_categorie']=$row->id_categorie;
			       	$donnees[$i]['id_client']=$row->id_client;
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['description']=$row->description;
			       	$donnees[$i]['image']=$row->image;
			       	$donnees[$i]['nb_participant']=$row->nb_participant;
			       	$donnees[$i]['date_debut']=$row->date_debut;
			       	$donnees[$i]['date_fin']=$row->date_fin;
			       	$donnees[$i]['heure_debut']=$row->heure_debut;
			       	$donnees[$i]['heure_fin']=$row->heure_fin;
			       	$donnees[$i]['lieu']=$row->lieu;
			       	$donnees[$i]['etat']=$row->etat;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}


			// fonction qui charge tous les evenements en cours
			
			public function findClientEvenementBd(){
				$data = $this->db->select('id,id_categorie,id_client,nom,description,image,nb_participant,date_debut,date_fin,heure_debut,heure_fin,lieu,etat')
								->from($this->table)
								->where('id_client',$this->id_client)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_categorie']=$row->id_categorie;
			       	$donnees[$i]['id_client']=$row->id_client;
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['description']=$row->description;
			       	$donnees[$i]['image']=$row->image;
			       	$donnees[$i]['nb_participant']=$row->nb_participant;
			       	$donnees[$i]['date_debut']=$row->date_debut;
			       	$donnees[$i]['date_fin']=$row->date_fin;
			       	$donnees[$i]['heure_debut']=$row->heure_debut;
			       	$donnees[$i]['heure_fin']=$row->heure_fin;
			       	$donnees[$i]['lieu']=$row->lieu;
			       	$donnees[$i]['etat']=$row->etat;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;
			}


			// fonction qui charge tous les evenements en cours
			
			public function findActiveEvenementBd(){
				$data = $this->db->select('id,id_categorie,id_client,nom,description,image,nb_participant,date_debut,date_fin,heure_debut,heure_fin,lieu,etat')
								->from($this->table)
								->where('etat','En Cours')
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_categorie']=$row->id_categorie;
			       	$donnees[$i]['id_client']=$row->id_client;
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['description']=$row->description;
			       	$donnees[$i]['image']=$row->image;
			       	$donnees[$i]['nb_participant']=$row->nb_participant;
			       	$donnees[$i]['date_debut']=$row->date_debut;
			       	$donnees[$i]['date_fin']=$row->date_fin;
			       	$donnees[$i]['heure_debut']=$row->heure_debut;
			       	$donnees[$i]['heure_fin']=$row->heure_fin;
			       	$donnees[$i]['lieu']=$row->lieu;
			       	$donnees[$i]['etat']=$row->etat;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;
			}

			// fonction qui charge tous les evenements Archives
			
			public function findArchiveEvenementBd(){
				$data = $this->db->select('id,id_categorie,id_client,nom,description,image,nb_participant,date_debut,date_fin,heure_debut,heure_fin,lieu,etat')
								->from($this->table)
								->where('etat','Archive')
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_categorie']=$row->id_categorie;
			       	$donnees[$i]['id_client']=$row->id_client;
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['description']=$row->description;
			       	$donnees[$i]['image']=$row->image;
			       	$donnees[$i]['nb_participant']=$row->nb_participant;
			       	$donnees[$i]['date_debut']=$row->date_debut;
			       	$donnees[$i]['date_fin']=$row->date_fin;
			       	$donnees[$i]['heure_debut']=$row->heure_debut;
			       	$donnees[$i]['heure_fin']=$row->heure_fin;
			       	$donnees[$i]['lieu']=$row->lieu;
			       	$donnees[$i]['etat']=$row->etat;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;
			}

			// fonction qui reccupère juste l'id_categorie d'un evenement

			public function findId_categorie($id){
				$data =$this->db->select('id_categorie')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

								
				foreach ($data as $row){
			       	$donnees['id_categorie']=$row->id_categorie;
				}

				return $donnees['id_categorie'];
			}


			public function findinfoEvent($id){
				$data =$this->db->select('id,id_categorie,id_client,nom,description,image,nb_participant,date_debut,date_fin,heure_debut,heure_fin,lieu,etat')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

								
				foreach ($data as $row){
			       	$donnees['id']=$row->id;
			       	$donnees['id_categorie']=$row->id_categorie;
			       	$donnees['id_client']=$row->id_client;
			       	$donnees['nom']=$row->nom;
			       	$donnees['description']=$row->description;
			       	$donnees['image']=$row->image;
			       	$donnees['nb_participant']=$row->nb_participant;
			       	$donnees['date_debut']=$row->date_debut;
			       	$donnees['date_fin']=$row->date_fin;
			       	$donnees['heure_debut']=$row->heure_debut;
			       	$donnees['heure_fin']=$row->heure_fin;
			       	$donnees['lieu']=$row->lieu;
			       	$donnees['etat']=$row->etat;
				}

				return $donnees;
			}

			// fonction qui reccupère juste la description d'un evenement

			public function findDescription($id){
				$data =$this->db->select('description')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

					$donnees['description']='non';				
				foreach ($data as $row){
			       	$donnees['description']=$row->description;
				}

				return $donnees['description'];
			}
			

		    // fonction qui reccupère juste le nom d'un evenement
			
			public function findEvenementNom($id){
				$data =$this->db->select('nom')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

				foreach ($data as $row){
			       	$donnees['nom']=$row->nom;
				}

				return $donnees['nom'];
			}

		    // fonction qui modifie juste l'etat d'un evenement
			
			// public function actualiseEtat($id,$etat){
			// 	$this->db->set('etat', $etat)
			//     	 ->where('id',$id)
			// 		 ->update($this->table);
			// }

		    // fonction qui modifie juste l'etat d'un evenement
			public function actualiseEtat(){
				$this->db->set('etat', $this->etat)
			    	 ->where('id',$this->id)
					 ->update($this->table);
			}

		    // fonction qui modifie juste la categorie d'un evenement
			public function actualiseCategorie(){
				$this->db->set('categorie', $this->categorie)
			    	 ->where('id',$this->id)
					 ->update($this->table);
			}

		    // fonction qui modifie juste le nom d'un evenement
			public function actualiseNom(){
				$this->db->set('nom', $this->nom)
			    	 ->where('id',$this->id)
					 ->update($this->table);
			}

		    // fonction qui modifie juste le description d'un evenement
			public function actualiseDescription(){
				$this->db->set('description', $this->description)
			    	 ->where('id',$this->id)
					 ->update($this->table);
			}

		    // fonction qui modifie juste l'image d'un evenement
			public function actualiseImage(){
				$this->db->set('image', $this->image)
			    	 ->where('id',$this->id)
					 ->update($this->table);
			}

		    // fonction qui modifie juste le nb_participant d'un evenement
			public function actualiseNb_participant(){
				$this->db->set('nb_participant', $this->nb_participant)
			    	 ->where('id',$this->id)
					 ->update($this->table);
			}

		    // fonction qui modifie juste la date_debut d'un evenement
			public function actualiseDate_debut(){
				$this->db->set('date_debut', $this->date_debut)
			    	 ->where('id',$this->id)
					 ->update($this->table);
			}

		    // fonction qui modifie juste la date_fin d'un evenement
			public function actualiseDate_fin(){
				$this->db->set('date_fin', $this->date_fin)
			    	 ->where('id',$this->id)
					 ->update($this->table);
			}

		    // fonction qui modifie juste l'heure_debut d'un evenement
			public function actualiseHeure_debut(){
				$this->db->set('heure_debut', $this->heure_debut)
			    	 ->where('id',$this->id)
					 ->update($this->table);
			}

		    // fonction qui modifie juste l'heure_fin d'un evenement
			public function actualiseHeure_fin(){
				$this->db->set('heure_fin', $this->heure_fin)
			    	 ->where('id',$this->id)
					 ->update($this->table);
			}

		    // fonction qui modifie juste le lieu d'un evenement
			public function actualiseLieu(){
				$this->db->set('lieu', $this->lieu)
			    	 ->where('id',$this->id)
					 ->update($this->table);
			}

			// fonction qui supprime un evenement
			public function delete(){  

				$this->db->where('id', $this->id)
						 ->delete($this->table);
						 
			}


			// setters


			public function setId($id){
				$this->id=$id;
			}


			public function setId_categorie($id_categorie){
				$this->id_categorie=$id_categorie;
			}

			public function setId_client($id_client){
				$this->id_client=$id_client;
			}
			
			public function setNom($nom){
				$this->nom=$nom;
			}

			public function setDescription($description){
				$this->description=$description;
			}

			public function setImage($image){
				$this->image=$image;
			}

			public function setNb_participant($nb_participant){
				$this->nb_participant=$nb_participant;
			}

			public function setDate_debut($date_debut){
				$this->date_debut=$date_debut;
			}

			public function setDate_fin($date_fin){
				$this->date_fin=$date_fin;
			}

			public function setHeure_debut($heure_debut){
				$this->heure_debut=$heure_debut;
			}

			public function setheure_fin($heure_fin){
				$this->heure_fin=$heure_fin;
			}

			public function setlieu($lieu){
				$this->lieu=$lieu;
			}
			public function setetat($etat){
				$this->etat=$etat;
			}
			

			// getteurs

			public function getId(){
				return $this->id;
			
			}

			
			public function getId_categorie(){
				return $this->id_categorie;
			
			}
			
			public function getId_client(){
				return $this->id_client;
			
			}

			public function getNom(){
				return $this->nom;
			
			}

			public function getDescription(){
				return $this->description;
			
			}
			public function getImage(){
				return $this->image;
			
			}
				public function getNb_participant(){
				return $this->nb_participant;
			
			}
				public function getDate_debut(){
				return $this->date_debut;
			
			}
				public function getDate_fin(){
				return $this->date_fin;
			
			}
				public function getHeure_debut(){
				return $this->heure_debut;
			
			}
				public function getHeure_fin(){
				return $this->heure_fin;
			
			}
				public function getLieu(){
				return $this->lieu;
			
			}
				public function getEtat(){
				return $this->etat;
			
			}
						
}


?>
