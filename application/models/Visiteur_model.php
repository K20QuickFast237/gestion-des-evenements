<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Visiteur_model extends CI_Model{
		
		function __construct()
			{
			
			}
		
			// gerer un Visiteur

			private $id;
			private $id_users;
			private $email;
			private $password;
			

			protected $table= 'visiteur';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}



			public function addVisiteur(){

			    // $this->db->set('id', $this->id)
			    $this->db->set('id_users', $this->id_users)
				    	 ->set('email', $this->email)
				    	 ->set('password', $this->password)
						 ->insert($this->table);
		
			}

			// fonction libere l'espace d'un visiteur ayant change de statut
			// public function moveto($new){

			// 	$this->db->set('email', $new)
			// 	    	 ->set('password', $new)
			// 	    	 ->where('email', $this->email)
			// 			 ->update($this->table);
		
			// }
			
			// fonction libere l'espace d'un visiteur ayant change de statut
			public function remove($email){  

				$this->db->where('email', $email)
						 ->delete($this->table);
						 
			}


			// fonction qui charge tous les Visiteurs pour faire le filtrage de donnee
			
			public function findAllVisiteurBd(){
				$data = $this->db->select('id,id_users,email,password')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_users']=$row->id_users;
			       	$donnees[$i]['email']=$row->email;
			       	$donnees[$i]['password']=$row->password;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}

			// fonction qui verifie l'existance d'un email parmi les Visiteur

			public function testExistemail($email){
				
				$data =$this->db->select('email')
								->from($this->table)
								->where('email', $email)
								->limit(1)
								->get()
								->result();

								
				foreach ($data as $row){
			       	$donnees['email']=$row->email;
				}

				if (isset($donnees)) {
					return 'ok';
				}else{
					return 'non';
				}
			}



		    // fonction qui reccupère juste l'identifiant utilisateur d'un Visiteur quelconque
			
			public function findIdUsers($id){
				$data =$this->db->select('id_users')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

				foreach ($data as $row){
			       	$donnees['id_users']=$row->id_users;
				}

				return $donnees['id_users'];
			}

		    // fonction qui reccupère juste le mot de passe d'un Visiteur quelconque
			
			public function findPassword($email){
				$data =$this->db->select('password')
								->from($this->table)
								->where('email', $email)
								->limit(1)
								->get()
								->result();

				foreach ($data as $row){
			       	$donnees['password']=$row->password;
				}

				return $donnees['password'];
			}



			// setteurs


			public function setId($id){
				$this->id=$id;
			}

			public function setId_users($id_users){
				$this->id_users=$id_users;
			}


			public function setNom($nom){
				$this->id_users=$id_users;
			}
			
			public function setEmail($email){
				$this->email=$email;
			}

			public function setPassword($password){
				$this->password=$password;
			}
			// public function setTelephone($telephone){
			// 	$this->telephone=$telephone;
			// }
			// public function setProfil($profil){
			// 	$this->profil=$profil;
			// }
			

			// getteurs

			public function getId(){
				return $this->id;
			
			}

			
			public function getId_users(){
				return $this->id_users;
			
			}

			public function getEmail(){
				return $this->email;
			
			}

			public function getPassword(){
				return $this->password;
			
			}
			// public function getTelephone(){
			// 	return $this->telephone;
			
			// }
			// 	public function getProfil(){
			// 	return $this->profil;
			
			// }
						
	
}


?>
